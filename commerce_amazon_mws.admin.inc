<?php

/**
 * @file commerce_amazon_mws.admin.inc
 */

/**
 * Builds the admin settings form for amazon mws.
 *
 * @param $form
 * @param $form_state
 *
 * @return mixed
 */
function commerce_amazon_mws_admin_form($form, &$form_state) {
  $form = array();

  $settings_fields = array(
    "access-key-id"       => 'AWS Access Key ID',
    "secret-access-key"   => 'Secret Access Key',
    "application-name"    => 'Application Name',
    "version-number"      => 'Version Number',
    "marketplace-id"      => 'Marketplace ID',
    "merchant-token"      => 'Merchant Token',
    "seller-id"           => 'Seller ID',
    "auth-token"          => 'MWS Auth Token',
  );

  $form['commerce_amazon_mws_enable_cron'] = array(
    '#type'           => 'checkbox',
    '#title'          => t('Enable Cron'),
    '#default_value'  => variable_get('commerce_amazon_mws_enable_cron'),
  );

  $form['commerce_amazon_mws_host_restrict'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Restrict Cron To Hostname'),
    '#description'    => t('Restrict module use to run cron on the given host
                          name, useful when running a staging and production
                          environment. Note this uses the gethostname() php
                          function, not the HTTP_HOST'),
    '#default_value'  => variable_get('commerce_amazon_mws_host_restrict', ''),
  );

  if (gethostname() == variable_get('commerce_amazon_mws_host_restrict', '')) {
    $form['commerce_amazon_mws_host_restrict']['#attributes'] = array('style' => array('border: 2px solid lime;'));
  }
  else {
    $form['commerce_amazon_mws_host_restrict']['#attributes'] = array('style' => array('border: 2px solid red;'));
  }

  $form['amazon-mws-production'] = array(
    '#type'         => 'fieldset',
    '#title'        => t('Production Settings'),
    '#collapsible'  => TRUE,
    '#collapsed'    => TRUE,
  );

  foreach ($settings_fields as $key => $val) {
    $form['amazon-mws-production']['commerce_amazon_mws_prod_' . $key] = array(
      '#type'           => 'textfield',
      '#title'          => t($val),
      '#default_value'  => t(variable_get('commerce_amazon_mws_prod_' . $key, '')),
    );
  }

  $form['amazon-mws-mapping'] = array(
    '#type'         => 'fieldset',
    '#title'        => t('Field Mapping'),
    '#collapsible'  => TRUE,
    '#collapsed'    => TRUE,
  );

  $form['amazon-mws-mapping']['amazon-mws-mapping'] = array(
    '#type'        => 'textarea',
    '#title'       => t('Mapping'),
    '#rows'        => 25,
    '#description' => t('json mappable field, this exports to
                        commerce_amazon_mws/includes/commerce_amazon_mws.map.json,
                        refer to the following document for help
                        https://images-na.ssl-images-amazon.com/images/G/01/rainier/help/XML_Documentation_Intl.pdf
                        along with the sample file.'),
    '#disabled'    => TRUE,
  );

  // Display json man file with valid/invalid flag.
  $map_json_path = drupal_get_path("module", "commerce_amazon_mws") . "/includes/commerce_amazon_mws.map.json";
  if (file_exists($map_json_path)) {
    $valid_json = FALSE;
    $contents = file_get_contents($map_json_path);

    try {
      $valid_json = json_decode($contents, TRUE);
    }
    // We can just skip over this exception as we display an error if the JSON isn't valid and hide some features.
    catch (Exception $e) {

    };

    if ($valid_json) {
      $form['amazon-mws-mapping']['amazon-mws-mapping']['#prefix'] =
        "<div style='color:green;font-weight:bold;font-size:14px;'>&#10004; File is valid json</div>";
      $form['amazon-mws-mapping']['amazon-mws-mapping']['#attributes']['style'] = 'background:#dbfbdb; color:#584462';
    }
    else {
      $form['amazon-mws-mapping']['amazon-mws-mapping']['#prefix'] =
        "<div style='color:red;font-weight:bold;font-size:14px;'>&#10005; File is not valid json</div>";
      $form['amazon-mws-mapping']['amazon-mws-mapping']['#attributes']['style'] = 'background:#ff8989; color: black;';
    }

    $form['amazon-mws-mapping']['amazon-mws-mapping']['#default_value'] = $contents;

    // Create a feed sampler selection if we get valid json to generate sample XML from a node of a display_type.
    if (isset($valid_json['display_node'])) {

      $form['amazon-mws-feed-sampler'] = array(
        '#type'         => 'fieldset',
        '#title'        => t('Feed Sample Generator'),
        '#description'  => t('Use this to generate some sample XML for a feed
                              type for a random Amazon list enabled display node.
                              You must have valid json to view the sample generator'),
        '#collapsible'  => TRUE,
        '#collapsed'    => FALSE,
      );

      $form['amazon-mws-feed-sampler']['sampler-feed-type'] = array(
        '#type'         => 'textfield',
        '#description'  => t('Case sensitive, check the json field mapping for options i.e Inventory, Price'),
        '#title'        => t('Feed Type'),
      );

      $form['amazon-mws-feed-sampler']['sampler-display-node-type'] = array(
        '#type'         => 'textfield',
        '#description'  => t('Case sensitive, check the json field mapping for options i.e. product_display'),
        '#title'        => t('Display Node Type'),
      );

      $form['amazon-mws-feed-sampler']['sampler-node-nid'] = array(
        '#type'         => 'textfield',
        '#description'  => t('If no node id is set, a random one will be chosen.
                              Note! When selecting a node id manually, the sampler
                              does not take the list flag into account.'),
        '#title'        => t('Node ID (Optional)'),
      );

      $form['amazon-mws-feed-sampler']['sampler-display-area'] = array(
        '#markup' => '<div id="amazon-xml-sample-display-area-wrapper"></div>',
      );

      $form['amazon-mws-feed-sampler']['sampler-submit'] = array(
        '#type'   => 'submit',
        '#value'  => t('Generate XML Sample'),
        '#ajax'   => array(
          'callback'  => 'commerce_amazon_mws_admin_generate_sample_feed_ajax',
          'wrapper'   => 'amazon-xml-sample-display-area-wrapper',
          'method'    => 'replace',
          'effect'    => 'fade',
        ),
        '#submit' => array(),
      );
    }
  }

  /*
   * include here:
   *  - Access Key ID
   *  - Secret Access Key
   *  - Application/Company Name
   *  - Version Number
   *  - Marketplace ID
   *  - Merchant ID
   */
  $settings_form = system_settings_form($form);
  $settings_form['#submit'][] = 'commerce_amazon_mws_admin_form_submit';

  return $settings_form;
}

/**
 * Form submit handler for commerce_amazon_mws_admin_form_submit.
 *
 * @param $form
 * @param $form_state
 */
function commerce_amazon_mws_admin_form_submit(&$form, &$form_state) {
  $values = $form_state['values'];
  $exclude = array('amazon-mws-field-mapping', 'sampler-display-node-type', 'sampler-feed-type', 'sampler-display-area');

  foreach ($values as $key => $val) {
    if (!in_array($key, $exclude) && !is_array($val) && !is_object($val)) {
      variable_set($key, $val);
    }
  }
}

/**
 * Handles the sample feed functionality.
 *
 * @param $form
 * @param $form_state
 *
 * @return string
 */
function commerce_amazon_mws_admin_generate_sample_feed_ajax($form, &$form_state) {
  module_load_include('inc', 'commerce_amazon_mws', 'includes/commerce_amazon_mws.feed_types');
  $display_node_type = $form_state['values']['sampler-display-node-type'];
  $feed_type = $form_state['values']['sampler-feed-type'];
  $node_nid = $form_state['values']['sampler-node-nid'];

  $sample = "<div style='background:#ff8989; color:#000; border: 1px solid red; padding: 15px; overflow-y: scroll; "
          . "'overflow-x: auto; font-style: italic;'>"
          . "No feed sample could be generated, check the error logs or whether or not you have a list "
          . "flag enabled for this feed type, if you do check to make sure at least one node is set to be "
          . "listed on Amazon otherwise no data will display. Also check that the Feed Type is and Display "
          . "Node Type is correct."
          . "</div>";

  if ($display_node_type && $feed_type) {
    // This should all have been verified in the admin panel otherwise the feed sample generator wouldn't display.
    $map_json_path = drupal_get_path("module", "commerce_amazon_mws") . "/includes/commerce_amazon_mws.map.json";
    $contents = file_get_contents($map_json_path);
    $json = json_decode($contents, TRUE);

    if (isset($json['display_node'][$display_node_type][$feed_type])) {

      // If no nid entered, select a random one.
      if (empty($node_nid)) {

        // Get a node of the specified display type.
        $query = new EntityFieldQuery();
        $query->entityCondition('entity_type', 'node');
        $query->propertyCondition('type', $display_node_type, '=');

        // If there's a list flag field for this feed type, constrain the entity query to only get nodes that are
        // set to list on Amazon MWS.
        $list_flag_field = NULL;
        if (isset($json['display_node'][$display_node_type][$feed_type]['list_flag'])) {
          $list_flag_field = $json['display_node'][$display_node_type][$feed_type]['list_flag'];
          $list_flag_field = str_replace("%", "", $list_flag_field);
          $query->fieldCondition($list_flag_field, 'value', 1, '=');
        }

        if ($data = $query->execute()) {
          $data = array_values($data['node']);
          $idx = rand(0, count($data) - 1);
          $node_nid = $data[$idx]->nid;
        }
      }

      if (!empty($node_nid)) {

        $service = new CommerceAmazonMWSTypeMapping($node_nid, $feed_type);
        $feed = new CommerceAmazonMwsFeeds();

        // Change the feed type here to match an actual amazon feed.
        switch ($feed_type) {
          case 'Inventory':
            $feed_type = INVENTORY_FEED;
            break;

          case 'ProductImage':
            $feed_type = PRODUCT_IMAGES_FEED;
            break;

          case 'Product':
            $feed_type = PRODUCT_FEED;
            break;

          case 'Price':
            $feed_type = PRICING_FEED;
            break;

          case 'Relationship':
            $feed_type = RELATIONSHIPS_FEED;
            break;
        }

        $sample  = "<div><em>" . t("Displaying feed generated for node") . "</em>: <strong>$node_nid</strong></div>";
        $sample .= "<div style='background:#dbfbdb; color:#584462; border: 1px solid lime; padding: 15px;"
                 . "overflow-y: scroll; overflow-x: auto;'>"
                 . "<pre>" . htmlentities($feed->FormatFeed($service->getTransposed(), $feed_type)) . "</pre>"
                 . "</div>";
      }
    }
  }

  return "<div id='amazon-xml-sample-display-area-wrapper'>$sample</div>";
}
