<?php

/**
 * @file commerce_amazon_mws.reports.inc
 * @description
 *  gives access to the report queue to pull
 *  sales and inventory and to compare alongside
 *  local inventory.
 */

/**
 * Implements hook_form().
 */
function commerce_amazon_mws_report_form($form, &$form_state) {
  module_load_include('inc', 'commerce_amazon_mws', '/includes/commerce_amazon_mws.report_types');

  commerce_amazon_mws_reports_check_queue();

  $form['#attached']['css'] = array(
    drupal_get_path('module', 'commerce_amazon_mws') . '/css/commerce_amazon_mws.reports.css',
  );

  // Queue a report.
  $form['amazon_mws_queue_report'] = array(
    '#type'         => 'fieldset',
    '#attributes'   => array('class' => array('amazon-mws--inline_container')),
    '#title'        => t('Queue Report'),
    '#collapsible'  => TRUE,
  );

  $form['amazon_mws_queue_report']['start_date'] = array(
    '#type'   => 'date',
    '#title'  => t('Start Date'),
    '#size'   => 15,
  );

  $form['amazon_mws_queue_report']['end_date'] = array(
    '#type'   => 'date',
    '#title'  => t('End Date'),
    '#size'   => 15,
  );

  $form['amazon_mws_queue_report']['report_type'] = array(
    '#type'     => 'select',
    '#title'    => t('Report Type'),
    '#options'  => array(
      SOLD_LISTINGS_REPORT => t('Sold Listings Report'),
      ACTIVE_LISTINGS_REPORT => t('Active Listings Report'),
      INVENTORY_REPORT => t('Inventory Report'),
    ),
  );

  $form['amazon_mws_queue_report']['queue_submit'] = array(
    '#type'   => 'submit',
    '#value'  => t('Queue Report'),
    '#size'   => 15,
  );

  /**
   * Vertical Tab Form
   */
  $form['commerce_amazon_mws_report_form']  = array(
    '#type' => 'vertical_tabs',
  );

  // Sold listings report.
  $form['amazon_mws_sold_listings_report'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Sold Listings Reports') . ' ('
    . commerce_amazon_mws_reports_report_type_count(SOLD_LISTINGS_REPORT) . ')',
    '#group' => 'commerce_amazon_mws_report_form',
  );

  $form['amazon_mws_sold_listings_report']['report_table'] = array(
    '#markup' => commerce_amazon_mws_reports_build_report_list_table(SOLD_LISTINGS_REPORT),
  );

  $form['amazon_mws_sold_listings_report']['action_button'] = array(
    '#type'   => 'button',
    '#value'  => t('delete'),
    '#attributes' => array('class' => array('commerce_amazon_mws--delete-reports')),
  );

  // Active listings report.
  $form['amazon_mws_active_listing_report'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Active Listings Reports') . ' ('
    . commerce_amazon_mws_reports_report_type_count(ACTIVE_LISTINGS_REPORT) . ')',
    '#group' => 'commerce_amazon_mws_report_form',
  );

  $form['amazon_mws_active_listing_report']['report_table'] = array(
    '#markup' => commerce_amazon_mws_reports_build_report_list_table(ACTIVE_LISTINGS_REPORT),
  );

  $form['amazon_mws_active_listing_report']['action_button'] = array(
    '#type'   => 'button',
    '#value'  => t('delete'),
    '#attributes' => array('class' => array('commerce_amazon_mws--delete-reports')),
  );

  // Inventory reporting.
  $form['amazon_mws_inventory_report'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Inventory Reports') . ' (' . commerce_amazon_mws_reports_report_type_count(INVENTORY_REPORT) . ')',
    '#group' => 'commerce_amazon_mws_report_form',
  );

  $form['amazon_mws_inventory_report']['report_table'] = array(
    '#markup' => commerce_amazon_mws_reports_build_report_list_table(INVENTORY_REPORT),
  );

  $form['amazon_mws_inventory_report']['action_button'] = array(
    '#type'   => 'button',
    '#value'  => t('delete'),
    '#attributes' => array('class' => array('commerce_amazon_mws--delete-reports')),
  );

  // Cron processed reports.
  // These are just the sales reports pulled by the cron when updating local
  // stock and are deleted by a cron job when they reach a particular age.
  $form['amazon_mws_cron_processed_report'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Cron Processed Reports') . ' (' . commerce_amazon_mws_reports_cron_processed_report_count() . ')',
    '#group' => 'commerce_amazon_mws_report_form',
    '#description' => '<em>'
    . t('These reports are run automatically by the cron, they are deleted automatically after a certain age.')
    . '</em>',
  );

  $form['amazon_mws_cron_processed_report']['report_table'] = array(
    '#markup' => commerce_amazon_mws_reports_build_cron_processed_report_list_table(),
  );

  // Queued reports.
  $form['amazon_mws_report_queue'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Report Queue') . ' (' . commerce_amazon_mws_reports_report_queue_count() . ')',
    '#group' => 'commerce_amazon_mws_report_form',
  );

  $form['amazon_mws_report_queue']['queue_table'] = array(
    '#markup' => commerce_amazon_mws_reports_build_report_queue_list(),
    '#suffix' => '<div><em>' . t('Refresh the page to update the queue status;
    reports are checked a minute after the last check.') . '</em></div>',
  );

  $form['access_token'] = array(
    '#type' => 'hidden',
    '#default_value' => drupal_get_token(),
    '#attributes' => array('id' => 'commerce_amazon_mws_access_token'),
  );

  $form['#attached']['js'] = array(
    drupal_get_path('module', 'commerce_amazon_mws') . '/js/commerce_amazon_mws.reports.js',
  );

  return $form;
}

/**
 * Checks the report queue, if a report is queued it is
 * requested, if it is available, it is removed from the
 * queue and stored in the reports. the queue check
 * only checks once every minute to not clog the api
 * up with requests.
 */
function commerce_amazon_mws_reports_check_queue() {
  $queues = commerce_amazon_mws_reports_report_types();
  $service = new CommerceAmazonMwsReports();
  $reports_to_check = array();

  foreach ($queues as $queue_type) {
    if ($queue = CommerceAmazonMWSCache::getCache(base64_encode($queue_type), 'report_system_queue')) {
      foreach ($queue as $row) {
        // If the last check timestamp is greater than a minute, check the report.
        if ((time() - $row['last_checked']) > 60) {
          $reports_to_check[] = $row['request_id'];
        }
      }
    }
  }

  if (count($reports_to_check)) {
    $result = $service->GetReportRequestList($reports_to_check);

    // If there's only one in the queue listed, set the info to an array of results, XML parse oddness.
    if (isset($result['GetReportRequestListResult']['ReportRequestInfo']['ReportRequestId'])) {
      $result['GetReportRequestListResult']['ReportRequestInfo'] =
        array($result['GetReportRequestListResult']['ReportRequestInfo']);
    }
    $result = $result['GetReportRequestListResult']['ReportRequestInfo'];

    // Loop results per report, if the status is done, get the report and cache it and remove from queue.
    foreach ($result as $row) {
      switch ($row['ReportProcessingStatus']) {

        case '_DONE_':
          // finished, remove the queued report, fetch and cache the report.
          commerce_amazon_mws_reports_remove_queued_report($row['ReportType'], $row['ReportRequestId']);
          commerce_amazon_mws_reports_save_report($row['ReportType'], $row['GeneratedReportId'], $row['StartDate'],
            $row['EndDate'], $row['CompletedDate']);
          break;

        case '_IN_PROGRESS_':
        case '_SUBMITTED_':
          // Still processing, reset the report timestamp.
          commerce_amazon_mws_reports_update_queued_report_timestamp($row['ReportType'], $row['ReportRequestId']);
          break;

        // Remove from the queue if we get an unexpected status.
        default:
          commerce_amazon_mws_reports_remove_queued_report($row['ReportType'], $row['ReportRequestId']);
          break;
      }
    }
  }
}

/**
 * Displays a report that was processed by the report queue cron.
 *
 * @param $report_id
 *
 * @return string
 *
 * @throws Exception
 */
function commerce_amazon_mws_reports_display_cron_processed_report($report_id) {
  drupal_add_css(drupal_get_path('module', 'commerce_amazon_mws') . '/css/commerce_amazon_mws.reports.css');

  $query = db_select('amazon_mws_cache', 'c')
    ->fields('c')
    ->condition('cache_type', 'report', '=')
    ->condition('cache_id', $report_id, '=')
    ->execute();

  if ($data = $query->fetchAssoc()) {
    $data = unserialize(base64_decode($data['data']));
    $header = array_keys($data[0]);
    return theme('table', array(
      'header' => $header,
      'rows' => $data,
      'sticky' => FALSE,
      'attributes' => array(
        'class' => 'amazon-mws--display_cron_report_table',
      ),
    ));
  }

  return 'The report with id: ' . $report_id . ' does not exist';
}

/**
 * Displays a report by report_id.
 *
 * @param $report_id
 *
 * @return mixed
 *
 * @throws Exception
 */
function commerce_amazon_mws_reports_display_report($report_id) {

  $report_display = '<div>' . t('Report not found') . '</div>';
  $form = array();

  if ($report = CommerceAmazonMWSCache::getCache($report_id, 'report_system_report')) {
    $header = array();
    foreach (array_keys($report[0]) as $key) {
      $header[] = array('data' => t($key));
    }
    $rows = array();

    $row_count                = 0;
    $matching_sku_count       = 0;
    $no_issues_count          = 0;
    $non_matching_sku_count   = 0;
    $title_mismatch_count     = 0;
    $non_listed_item_count    = 0;

    // Get the sku key for the data, if it's still NULL, we'll skip over the
    // SKU matching logic in the loop below.
    $sku_key = NULL;
    if (array_search('seller-sku', array_keys($report[0])) !== FALSE) {
      $sku_key = 'seller-sku';
    }
    elseif (array_search('sku', array_keys($report[0])) !== FALSE) {
      $sku_key = 'sku';
    }

    // If there's a sku in the report, we'll loop every row for a Drupal sku match and
    // add a sku-match class to the row, as well as a clickable link to the product.
    foreach ($report as $row) {
      // On some instances there might be an empty row due to all of the XML parsing involved etc, skip these.
      if (!$row) {
        continue;
      }

      $seller_sku = (isset($row[$sku_key])) ? $row[$sku_key] : NULL;
      if ($seller_sku && $product = commerce_product_load_by_sku($seller_sku)) {
        $no_issues = TRUE;

        // 1. add a link to the product in the table including a sku-match class.
        $row[$sku_key] = l($row[$sku_key], 'admin/commerce/products/' . $product->product_id);
        $data = array('data' => $row, 'class' => array('sku-match'));

        // 2. if there's a product title, we'll need to check that the title matches with the local title,
        //    if it doesn't it could indicate that Amazon has possibly listed the item wrong.
        if (in_array('item-name', $header)) {
          if (!empty($row['item-name']) && $row['item-name'] != $product->title) {
            $data['data']['item-name'] = check_plain($row['item-name'])
              . '<div class="differing-title">' . check_plain($product->title) . '</div>';
            $data['class'][] = 'title-mismatch';
            $no_issues = FALSE;
            $title_mismatch_count++;
          }
        }

        // 3. if the parent node isn't set to be listed on Amazon, but it is listed, add a class to signify it.
        if ($parent_nid = commerce_amazon_mws_get_referencing_node_id($product)) {
          $mapping = new CommerceAmazonMWSTypeMapping($parent_nid);
          if (!$mapping->isListed()) {
            $data['class'][] = 'not-listed';
            $no_issues = FALSE;
            $non_listed_item_count++;
          }
        }

        // 4. add a class if there's no detected issues.
        if ($no_issues) {
          $data['class'][] = 'no-issues';
          $no_issues_count++;
        }

        $matching_sku_count++;
        $rows[] = $data;
      }
      // If there's no sku match, just add the row.
      else {
        $non_matching_sku_count++;
        // Only add the class if there's a sku key for this report.
        if ($sku_key) {
          $rows[] = array('data' => $row, 'class' => array('no-sku-match'));
        }
      }

      $row_count++;
    }

    $report_display = theme('table',
      array(
        'header' => $header,
        'rows' => $rows,
        'sticky' => FALSE,
        'attributes' => array(
          'class' => 'amazon-mws--display_report_table',
          'id' => 'amazon-mws-report',
        ),
      )
    );
  }
  $form['amazon_mws_report_table'] = array(
    '#markup' => $report_display,
    '#prefix' =>
    '<div class="row-keys">'
    . '<div class="row-key show-all"><strong>' . t('Show All') . '</strong>: ' . $row_count . '</div>'
    . '<div class="row-key no-issues"><strong>' . t('No Issues') . '</strong>: ' . $no_issues_count . '</div>'
    . '<div class="row-key no-sku-match"><strong>' . t('No Matching SKU') . '</strong>: ' . $non_matching_sku_count . '</div>'
    . '<div class="row-key not-listed"><strong>' . t('Item Not Listed') . '</strong>: ' . $non_listed_item_count . '</div>'
    . '<div class="row-key title-mismatch"><strong>' . t('Item Title Mismatch') . '</strong>: ' . $title_mismatch_count . '</div>'
    . '</div>',
  );

  $form['#attached']['css'] = array(
    drupal_get_path('module', 'commerce_amazon_mws') . '/css/commerce_amazon_mws.reports.css',
  );
  $form['#attached']['js'] = array(
    drupal_get_path('module', 'commerce_amazon_mws') . '/js/commerce_amazon_mws.reports.js',
  );

  return system_settings_form($form);
}

/**
 * Gets a finished report and caches it.
 *
 * @param $report_type
 * @param $report_id
 */
function commerce_amazon_mws_reports_save_report($report_type, $report_id, $start_date, $end_date, $completed_date) {
  $service = new CommerceAmazonMwsReports();
  if ($report = $service->GetReport($report_id)) {
    CommerceAmazonMWSCache::setCache($report_id, 'report_system_report', $report);

    // Map the report to a report caching map for this report type.
    $report_map = CommerceAmazonMWSCache::getCache(base64_encode($report_type), 'report_system_map');
    $report_map = ($report_map) ? $report_map : array();
    $report_map[] = array($report_id, $start_date, $end_date, $completed_date);

    CommerceAmazonMWSCache::setCache(base64_encode($report_type), 'report_system_map', $report_map);
  }
}

/**
 * Sets the timestamp again on a queued report that is still in process.
 *
 * @param $report_type
 * @param $request_id
 */
function commerce_amazon_mws_reports_update_queued_report_timestamp($report_type, $request_id) {
  if ($queue = CommerceAmazonMWSCache::getCache(base64_encode($report_type), 'report_system_queue')) {
    foreach ($queue as $idx => $row) {
      if ($row['request_id'] == $request_id) {
        $queue[$idx]['last_checked'] = time();
      }
    }
    // Save the queue back.
    CommerceAmazonMWSCache::setCache(base64_encode($report_type), 'report_system_queue', $queue);
  }
}

/**
 * Remove the queued report from the amazon reports cache if it's cached.
 *
 * @param $report_type
 * @param $request_id
 */
function commerce_amazon_mws_reports_remove_queued_report($report_type, $request_id) {
  if ($queue = CommerceAmazonMWSCache::getCache(base64_encode($report_type), 'report_system_queue')) {
    foreach ($queue as $idx => $row) {
      if ($row['request_id'] == $request_id) {
        unset($queue[$idx]);
      }
    }
    // Save the queue back.
    CommerceAmazonMWSCache::setCache(base64_encode($report_type), 'report_system_queue', $queue);
  }
}

/**
 * Returns valid report types.
 *
 * @return array
 */
function commerce_amazon_mws_reports_report_types() {
  return array(SOLD_LISTINGS_REPORT, ACTIVE_LISTINGS_REPORT, INVENTORY_REPORT);
}

/**
 * Returns a list of all the reports currently queued.
 *
 * @return string
 */
function commerce_amazon_mws_reports_build_report_queue_list() {
  module_load_include('inc', 'commerce_amazon_mws', '/includes/commerce_amazon_mws.report_types');
  $queues = commerce_amazon_mws_reports_report_types();

  $header = array(t('Request ID'), t('Report Type'), t('Last Time Checked'));
  $rows = array();

  foreach ($queues as $queue_type) {
    if ($queue = CommerceAmazonMWSCache::getCache(base64_encode($queue_type), 'report_system_queue')) {
      foreach ($queue as $row) {
        $rows[] = array(check_plain($row['request_id']), t($row['report_type']), date('Y-m-d H:i:s', $row['last_checked']));
      }
    }
  }

  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Return a total count of reports run by cron.
 *
 * @return int
 */
function commerce_amazon_mws_reports_cron_processed_report_count() {
  $count = 0;
  $data = db_select('amazon_mws_cache', 'c')
    ->fields('c')
    ->condition('cache_type', 'report', '=')
    ->execute();

  if ($data) {
    while ($data->fetchAssoc()) {
      $count++;
    }
  }

  return $count;
}

/**
 * Creates a table of processed reports that were gathered
 * during the report queue cron.
 *
 * @return string
 */
function commerce_amazon_mws_reports_build_cron_processed_report_list_table() {
  $header = array(t('Report Id'), t('Date'));
  $rows = array();

  $data = db_select('amazon_mws_cache', 'c')
    ->fields('c')
    ->condition('cache_type', 'report', '=')
    ->execute()
    ->fetchAllAssoc('cache_id');

  if ($data) {
    // Sort reports most recent first.
    rsort($data);
    foreach ($data as $idx => $row) {
      $rows[$idx][0] = l($row->cache_id, '/admin/reports/commerce_amazon_mws/cron/report/' . $row->cache_id);
      $rows[$idx][1] = date('c', $row->timestamp);
    }
  }
  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Builds a report list table.
 *
 * @param $report_type
 *
 * @return string
 */
function commerce_amazon_mws_reports_build_report_list_table($report_type) {
  $header = array(t('Report Id'), t('Start Date'), t('End Date'), t('Completed Date'), '');
  $rows = array();
  $REPORT_ID_COL = 0;
  $SELECT_COL = 4;

  if ($map = CommerceAmazonMWSCache::getCache(base64_encode($report_type), 'report_system_map')) {
    foreach ($map as $row) {
      $row[$SELECT_COL] = '<input type="checkbox" data-report-id="' . $row[$REPORT_ID_COL]
        . '" data-report-type="' . $report_type . '">';

      // The first col is a report id, turn it into a link.
      $row[$REPORT_ID_COL] = l($row[$REPORT_ID_COL], '/admin/reports/commerce_amazon_mws/report/' . $row[$REPORT_ID_COL]);
      $rows[] = $row;
    }
  }
  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Returns a count of reports for a given report type.
 *
 * @param $report_type
 *
 * @return int
 */
function commerce_amazon_mws_reports_report_type_count($report_type) {
  if ($map = CommerceAmazonMWSCache::getCache(base64_encode($report_type), 'report_system_map')) {
    return count($map);
  }
  return 0;
}

/**
 * Returns a count of reports currently queued.
 *
 * @return int
 */
function commerce_amazon_mws_reports_report_queue_count() {
  module_load_include('inc', 'commerce_amazon_mws', '/includes/commerce_amazon_mws.report_types');
  $queues = commerce_amazon_mws_reports_report_types();

  $count = 0;
  foreach ($queues as $queue_type) {
    $queue = CommerceAmazonMWSCache::getCache(base64_encode($queue_type), 'report_system_queue');
    $count += count($queue);
  }

  return $count;
}

/**
 * Implements hook_form_submit().
 */
function commerce_amazon_mws_report_form_submit(&$form, &$form_state) {
  // Queue report submit values.
  $start_date_vals = $form_state['values']['start_date'];
  $end_date_vals = $form_state['values']['end_date'];
  $report_type = $form_state['values']['report_type'];

  $start = date('c', mktime(0, 0, 0, $start_date_vals['month'], $start_date_vals['day'], $start_date_vals['year']));
  $end = date('c', mktime(0, 0, 0, $end_date_vals['month'], $end_date_vals['day'], $end_date_vals['year']));
  $service = new CommerceAmazonMwsReports();

  // Request the report and store the request result in the queue.
  $result = $service->RequestReport($report_type, $start, $end);
  if (!empty($result['RequestReportResult']['ReportRequestInfo'])) {
    // base64 encode the report type as: cache id.
    $report_type_queue = CommerceAmazonMWSCache::getCache(base64_encode($report_type), 'report_system_queue');
    $report_type_queue = ($report_type_queue) ? $report_type_queue : array();
    $report_type_queue[] = array(
      'request_id'  => $result['RequestReportResult']['ReportRequestInfo']['ReportRequestId'],
      'report_type' => $report_type,
      // Add a last checked timestamp, the queue is checked on refresh after a minute has passed per last checked.
      'last_checked' => time(),
    );
    CommerceAmazonMWSCache::setCache(base64_encode($report_type), 'report_system_queue', $report_type_queue);
  }
}

/**
 * Gets the referencing node id for a product, code taken from Laurens Meurs @:
 * https://drupalcommerce.org/questions/3176/how-get-node-id-product-display-using-product-id.
 *
 * @param $product
 *
 * @return bool|mixed
 */
function commerce_amazon_mws_get_referencing_node_id($product) {
  // Iterate through fields which refer to products.
  foreach (commerce_info_fields('commerce_product_reference') as $field) {
    // Build query.
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node', '=')
      ->fieldCondition($field['field_name'], 'product_id', $product->product_id, '=')
      ->range(0, 1);

    if ($result = $query->execute()) {
      // Return node id.
      return array_shift(array_keys($result['node']));
    }
  }

  return FALSE;
}

/**
 * Deletes reports that have been selected for deletion.
 *
 * @throws Exception
 */
function commerce_amazon_mws_reports_delete_report() {
  module_load_include('inc', 'commerce_amazon_mws', '/includes/commerce_amazon_mws.report_types');
  $report_id = $_GET['report_id'];
  $report_type = $_GET['report_type'];
  $token = $_GET['commerce_amazon_mws_access_token'];

  if (drupal_valid_token($token)) {
    // If system report exists.
    if (CommerceAmazonMWSCache::getCache($report_id, 'report_system_report')) {
      CommerceAmazonMWSCache::deleteCache($report_id, 'report_system_report');
    }

    // If report map exists.
    if ($map = CommerceAmazonMWSCache::getCache(base64_encode($report_type), 'report_system_map')) {
      foreach ($map as $idx => $report_map) {
        if ($report_map[0] == $report_id) {
          unset($map[$idx]);
          CommerceAmazonMWSCache::setCache(base64_encode($report_type), 'report_system_map', $map);
        }
      }
      drupal_json_output(array('ok' => TRUE));
      return;
    }
    drupal_json_output(array('ok' => FALSE));
  }
  else {
    drupal_access_denied();
  }
}
