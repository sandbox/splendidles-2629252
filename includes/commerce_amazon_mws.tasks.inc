<?php

/**
 * @file commerce_amazon_mws.tasks.inc
 */

define('COMMERCE_AMAZONMWS_REPORT', 'report');
define('COMMERCE_AMAZONMWS_QUEUE', 'queue');
define('COMMERCE_AMAZONMWS_STATUS_DONE', '_DONE_');
define('COMMERCE_AMAZONMWS_STATUS_SUBMITTED', '_SUBMITTED_');
define('COMMERCE_AMAZONMWS_STATUS_IN_PROGRESS', '_IN_PROGRESS_');

/**
 * Queues up various reports which will then be
 * checked in the sync reports function.
 */
function commerce_amazon_mws_queue_reports() {
  module_load_include('inc', 'commerce_amazon_mws', 'includes/commerce_amazon_mws.report_types');
  $service = new CommerceAmazonMwsReports();

  // Only run if cron is enabled for host.
  if (!$service->cron_enabled() || !$service->host_enabled()) {
    return;
  }

  $report_queue = CommerceAmazonMWSCache::getCache(COMMERCE_AMAZONMWS_QUEUE, COMMERCE_AMAZONMWS_QUEUE);
  $report_queue = ($report_queue) ? $report_queue : array();

  // Get the last time the report was run, or 15 minutes before, this is so that there
  // is never any skipped time should there for some reason be a delay or halt.
  $last_time_run = variable_get('commerce_amazon_mws_report_last_run', strtotime('-15 minutes'));
  $time_now = strtotime('now');
  variable_set('commerce_amazon_mws_report_last_run', $time_now);

  // This is an ordered list of reports to queue, the resulting report id's will
  // be added to the queue cache. Use a single string entry for a report with no
  // options, or a key/pair array for options to pass to the report, to make
  // this work properly the parameters should match the request report parameter
  // names in order, check the RequestReport parameter order for this under the
  // CommerceCommerceAmazonMWSReports class.
  $reports_to_queue = array(

    // Run an order report starting from the last time run, this will be used to
    // update local stock quantities from any orders made.
    SOLD_LISTINGS_REPORT => array(
      'startDate' => gmdate('c', $last_time_run),
      'endDate' => gmdate('c', $time_now),
    ),

  );

  foreach ($reports_to_queue as $report_type => $report_parameters) {

    // Report with no parameters.
    if (is_string($report_parameters)) {
      $report_type = $report_parameters;
      $report_request = $service->RequestReport($report_type);
    }
    // Report with parameters.
    else {
      // Add the report type as the first parameter.
      array_unshift($report_parameters, $report_type);
      $report_request = call_user_func_array(array($service, 'RequestReport'), $report_parameters);
    }

    // Add queue to report queue if request id is set.
    if (isset($report_request['RequestReportResult']['ReportRequestInfo']['ReportRequestId'])) {
      $report_queue[] = $report_request['RequestReportResult']['ReportRequestInfo']['ReportRequestId'];
    }
  }
  // Add the queue to the queue cache; make sure it's unique to avoid duplicates.
  CommerceAmazonMWSCache::setCache(COMMERCE_AMAZONMWS_QUEUE, COMMERCE_AMAZONMWS_QUEUE, array_unique($report_queue));
}

/**
 * Checks the status of each queued report, if the
 * report is flagged as complete by amazon, and is
 * of a given type, will perform a given function.
 */
function commerce_amazon_mws_sync_reports() {
  $service = new CommerceAmazonMwsReports();

  // Only run if cron is enabled for host.
  if (!$service->cron_enabled() || !$service->host_enabled()) {
    return;
  }

  if ($report_queue = CommerceAmazonMWSCache::getCache(COMMERCE_AMAZONMWS_QUEUE, COMMERCE_AMAZONMWS_QUEUE)) {

    $report_request_list = array();

    // Report to numerical array (this gets converted to an id list, so
    // numerical needs to be forced).
    $report_queue = array_values($report_queue);

    // Save on api calls, get the status for all queued reports.
    if ($request_list = $service->GetReportRequestList($report_queue)) {
      // If we get only one entry, make sure it's nested in the results as it
      // doesn't come pre-nested.
      if (isset($request_list['GetReportRequestListResult']['ReportRequestInfo']['ReportRequestId'])) {
        $report_request_list[] = $request_list['GetReportRequestListResult']['ReportRequestInfo'];
      }
      else {
        $report_request_list = $request_list['GetReportRequestListResult']['ReportRequestInfo'];
      }
    }

    foreach ($report_request_list as $report_request) {
      // If the report status is done, remove it from the queue.
      switch ($report_request['ReportProcessingStatus']) {

        case COMMERCE_AMAZONMWS_STATUS_SUBMITTED:
        case COMMERCE_AMAZONMWS_STATUS_IN_PROGRESS:
          // Pass on submitted; it's still processing.
          break;

        case COMMERCE_AMAZONMWS_STATUS_DONE:
          // Do something with the report, then fall through to default to
          // remove the report from the queue.
          _commerce_amazon_mws_process_report($report_request['GeneratedReportId'], $report_request['ReportType']);
          break;

        default:
          // Remove the report from the queue.
          foreach ($report_queue as $idx => $report_id) {
            if ($report_id == $report_request['ReportRequestId']) {
              unset($report_queue[$idx]);
            }
          }
          break;

      }
      CommerceAmazonMWSCache::setCache(COMMERCE_AMAZONMWS_QUEUE, COMMERCE_AMAZONMWS_QUEUE, $report_queue);
    }

  }
}

/**
 * Takes all of the items that are listed as
 * having a list flag under the Inventory feed, if there
 * is no flag, all items will be synced for stock regardless.
 */
function commerce_amazon_mws_sync_remote_stock_quantities() {
  $service = new CommerceAmazonMWSService();

  // Only run if cron is enabled for host.
  if (!$service->cron_enabled() || !$service->host_enabled()) {
    return;
  }

  $path = drupal_get_path('module', 'commerce_amazon_mws') . '/includes/commerce_amazon_mws.map.json';
  $items = array();

  if ($mapping = json_decode(file_get_contents($path), TRUE)) {

    foreach ($mapping['display_node'] as $node_type => $feed_types) {

      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'node');
      $query->propertyCondition('type', $node_type);

      // Add a constraint if there is a list flag on the Inventory field for
      // this display type !note this is done a second time per variation when
      // transposing, but this just prevents passing in every node the first
      // time around.
      if (isset($feed_types['Inventory']['list_flag'])) {
        $list_flag_field = str_replace('%', '', $feed_types['Inventory']['list_flag']);
        $query->fieldCondition($list_flag_field, 'value', 1, '=');
      }

      // Combine the display nodes with the items, these will be synced.
      if ($data = $query->execute()) {
        $items += array_keys($data['node']);
      }
    }
  }

  if (!empty($items)) {
    // Remove any possible duplicates, if any, then sync.
    $items = array_unique($items);
    _commerce_amazon_mws_task_update_multiple_remote_items_stock($items);
  }
}

/**
 * Syncs all of the products.
 */
function commerce_amazon_mws_sync_products() {
  $service = new CommerceAmazonMWSService();

  // Only run if cron is enabled for host.
  if (!$service->cron_enabled() || !$service->host_enabled()) {
    return;
  }

  $path = drupal_get_path('module', 'commerce_amazon_mws') . '/includes/commerce_amazon_mws.map.json';
  $items = array();

  if ($mapping = json_decode(file_get_contents($path), TRUE)) {

    foreach ($mapping['display_node'] as $node_type => $feed_types) {

      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'node');
      $query->propertyCondition('type', $node_type);

      // Add a constraint if there is a list flag on the Inventory field for
      // this display type !note this is done a second time per variation when
      // transposing, but this just prevents passing in every node the first
      // time around.
      if (isset($feed_types['Product']['list_flag'])) {
        $list_flag_field = str_replace('%', '', $feed_types['Product']['list_flag']);
        $query->fieldCondition($list_flag_field, 'value', 1, '=');
      }

      // Combine the display nodes with the items, these will be synced.
      if ($data = $query->execute()) {
        $items += array_keys($data['node']);
      }
    }
  }

  if (!empty($items)) {
    // Remove any possible duplicates, if any, then sync.
    $items = array_unique($items);

    // Sync items, relationships, images and prices all at once, the items
    // actually have to be uploaded first before updating images and item
    // prices, so these will be uploaded the second go around during the cron.
    _commerce_amazon_mws_task_upload_multiple_items($items);
    _commerce_amazon_mws_task_upload_item_relationships($items);
    _commerce_amazon_mws_task_upload_multiple_product_images($items);
    _commerce_amazon_mws_task_update_multiple_item_prices($items);
  }
}

/**
 * Clears out any cached reports exceeding the set cache lifetime.
 */
function commerce_amazon_mws_rotate_cached_reports() {
  $service = new CommerceAmazonMWSService();

  // Only run if cron is enabled for host.
  if (!$service->cron_enabled() || !$service->host_enabled()) {
    return;
  }

  CommerceAmazonMWSCache::cleanCache(NULL, COMMERCE_AMAZONMWS_REPORT);
}

/**
 * Gets the actual report, either from the cache if it is there, or from Amazon.
 * The logic for the report is dependent on the report type.
 *
 * @param $generated_report_id
 * @param $report_type
 */
function _commerce_amazon_mws_process_report($generated_report_id, $report_type) {
  module_load_include('inc', 'commerce_amazon_mws', 'includes/commerce_amazon_mws.report_types');
  if ($report = _commerce_amazon_mws_get_report($generated_report_id)) {

    switch ($report_type) {
      // Update stock for sales.
      case SOLD_LISTINGS_REPORT:
        foreach ($report as $item) {
          if (is_array($item)) {
            _commerce_amazon_mws_task_update_local_item_stock($item);
          }
        }
        break;

      default:
        break;

    }
  }
}

/**
 * Maps, transposes and submits a feed for a product item upload
 * to Amazon MWS.
 *
 * @param $item - a product display node
 */
function _commerce_amazon_mws_task_upload_new_item($item) {

  if ($mapping = new CommerceAmazonMWSTypeMapping($item->nid)) {
    module_load_include('inc', 'commerce_amazon_mws', 'includes/commerce_amazon_mws.feed_types');

    $service = new CommerceAmazonMwsFeeds();
    $xml = $service->FormatFeed($mapping->getTransposed(), PRODUCT_FEED);
    $tmp_xml_file = file_directory_temp() . "/mws_product" . time();

    if ($file = file_unmanaged_save_data($xml, $tmp_xml_file, FILE_EXISTS_RENAME)) {
      $openfile = fopen($file, 'r');
      $service->SubmitFeed($openfile, PRODUCT_FEED, $service->marketplace_id());

      fclose($openfile);
      file_unmanaged_delete($file);
    }
    else {
      watchdog("CommerceAmazonMWS", "%function: unable to write feed to temporary directory; check file permissions",
        array("%function" => __FUNCTION__), WATCHDOG_ERROR);
    }
  }
}

/**
 * Transposes multiple items by nid and submits them as a product feed.
 * Only items that have a list flag enabled or no list flag field will
 * be uploaded, which is done via the transpose function for the mapping.
 *
 * @param array $items
 */
function _commerce_amazon_mws_task_upload_multiple_items($items = array()) {
  module_load_include('inc', 'commerce_amazon_mws', 'includes/commerce_amazon_mws.feed_types');
  $products = array();

  // Get transposed entries for each variation.
  foreach ($items as $nid) {
    if ($mapping = new CommerceAmazonMWSTypeMapping($nid)) {
      foreach ($mapping->getTransposed() as $variation) {
        $products[] = $variation;
      }
    }
  }

  // Convert all of the transposed products to an XML product feed and submit it.
  if (count($products)) {
    $service = new CommerceAmazonMwsFeeds();
    $xml = $service->FormatFeed($products, PRODUCT_FEED);
    $tmp_xml_file = file_directory_temp() . "/mws_product" . time();

    if ($file = file_unmanaged_save_data($xml, $tmp_xml_file, FILE_EXISTS_RENAME)) {
      $openfile = fopen($file, 'r');
      $service->SubmitFeed($openfile, PRODUCT_FEED, $service->marketplace_id());

      fclose($openfile);
      file_unmanaged_delete($file);
    }
    else {
      watchdog("CommerceAmazonMWS", "%function: unable to write feed to temporary directory; check file permissions",
        array("%function" => __FUNCTION__), WATCHDOG_ERROR);
    }
  }
}

/**
 * Transposes multiple items by nid and submits them as a relationship feed.
 * Only items that have a list flag enabled or no list flag field will
 * be uploaded, which is done via the transpose function for the mapping.
 *
 * @param array $items
 */
function _commerce_amazon_mws_task_upload_item_relationships($items = array()) {

  module_load_include('inc', 'commerce_amazon_mws', 'includes/commerce_amazon_mws.feed_types');
  $products = array();

  // Get transposed entries for each variation.
  foreach ($items as $nid) {
    if ($mapping = new CommerceAmazonMWSTypeMapping($nid, "Relationship")) {
      foreach ($mapping->getTransposed() as $variation) {
        $products[] = $variation;
      }
    }
  }

  // Convert all of the transposed products to an XML product feed and submit it.
  if (count($products)) {
    $service = new CommerceAmazonMwsFeeds();
    $xml = $service->FormatFeed($products, RELATIONSHIPS_FEED);
    $tmp_xml_file = file_directory_temp() . "/mws_relationship" . time();

    if ($file = file_unmanaged_save_data($xml, $tmp_xml_file, FILE_EXISTS_RENAME)) {
      $openfile = fopen($file, 'r');
      $service->SubmitFeed($openfile, RELATIONSHIPS_FEED, $service->marketplace_id());

      fclose($openfile);
      file_unmanaged_delete($file);
    }
    else {
      watchdog("CommerceAmazonMWS", "%function: unable to write feed to temporary directory; check file permissions",
        array("%function" => __FUNCTION__), WATCHDOG_ERROR);
    }
  }
}

/**
 * Transpose multiple items by nid and submit them as a product feed.
 *
 * @param array $items
 */
function _commerce_amazon_mws_task_upload_multiple_product_images($items = array()) {

  module_load_include('inc', 'commerce_amazon_mws', 'includes/commerce_amazon_mws.feed_types');
  $products = array();

  // Get transposed entries for each variation.
  foreach ($items as $nid) {
    if ($mapping = new CommerceAmazonMWSTypeMapping($nid, "ProductImage")) {
      foreach ($mapping->getTransposed() as $variation) {
        $products[] = $variation;
      }
    }
  }

  // Convert all of the transposed products to an XML product images feed and submit it.
  if (count($products)) {
    $service = new CommerceAmazonMwsFeeds();
    $xml = $service->FormatFeed($products, PRODUCT_IMAGES_FEED);
    $tmp_xml_file = file_directory_temp() . "/mws_images" . time();

    if ($file = file_unmanaged_save_data($xml, $tmp_xml_file, FILE_EXISTS_RENAME)) {
      $openfile = fopen($file, 'r');
      $service->SubmitFeed($openfile, PRODUCT_IMAGES_FEED, $service->marketplace_id());

      fclose($openfile);
      file_unmanaged_delete($file);
    }
    else {
      watchdog("CommerceAmazonMWS", "%function: unable to write feed to temporary directory; check file permissions",
        array("%function" => __FUNCTION__), WATCHDOG_ERROR);
    }
  }
}

/**
 * Maps, transposes and submits the product images feed for an item.
 *
 * @param $item - a product display node
 */
function _commerce_amazon_mws_task_upload_product_image($item) {

  if ($mapping = new CommerceAmazonMWSTypeMapping($item->nid, "ProductImage")) {
    module_load_include('inc', 'commerce_amazon_mws', 'includes/commerce_amazon_mws.feed_types');

    $service = new CommerceAmazonMwsFeeds();
    $xml = $service->FormatFeed($mapping->getTransposed(), PRODUCT_IMAGES_FEED);
    $tmp_xml_file = file_directory_temp() . "/mws_images" . time();

    if ($file = file_unmanaged_save_data($xml, $tmp_xml_file, FILE_EXISTS_RENAME)) {
      $openfile = fopen($file, 'r');
      $service->SubmitFeed($openfile, PRODUCT_IMAGES_FEED, $service->marketplace_id());

      fclose($openfile);
      file_unmanaged_delete($file);
    }
    else {
      watchdog("CommerceAmazonMWS", "%function: unable to write feed to temporary directory; check file permissions",
        array("%function" => __FUNCTION__), WATCHDOG_ERROR);
    }
  }
}

/**
 * Updates the stock quantities of multiple remote items from local stock.
 *
 * @param $items
 *
 * @return bool
 */
function _commerce_amazon_mws_task_update_multiple_remote_items_stock($items = array()) {

  module_load_include('inc', 'commerce_amazon_mws', 'includes/commerce_amazon_mws.feed_types');
  $products = array();

  // Get transposed entries for each variation.
  foreach ($items as $nid) {
    if ($mapping = new CommerceAmazonMWSTypeMapping($nid, "Inventory")) {
      foreach ($mapping->getTransposed() as $variation) {
        $products[] = $variation;
      }
    }
  }

  // Convert all of the transposed products to an XML inventory feed and submit it.
  if (count($products)) {
    $service = new CommerceAmazonMwsFeeds();
    $xml = $service->FormatFeed($products, INVENTORY_FEED);
    $tmp_xml_file = file_directory_temp() . "/mws_inventory" . time();

    if ($file = file_unmanaged_save_data($xml, $tmp_xml_file, FILE_EXISTS_RENAME)) {
      $openfile = fopen($file, 'r');
      $service->SubmitFeed($openfile, INVENTORY_FEED, $service->marketplace_id());

      fclose($openfile);
      file_unmanaged_delete($file);
    }
    else {
      watchdog("CommerceAmazonMWS", "%function: unable to write feed to temporary directory; check file permissions",
        array("%function" => __FUNCTION__), WATCHDOG_ERROR);
    }
  }
}

/**
 * Updates the stock quantities of remote items from local stock.
 *
 * @param $node
 *
 * @return bool
 */
function _commerce_amazon_mws_task_update_remote_item_stock($node) {

  if ($mapping = new CommerceAmazonMWSTypeMapping($node->nid, "Inventory")) {
    module_load_include('inc', 'commerce_amazon_mws', 'includes/commerce_amazon_mws.feed_types');

    $service = new CommerceAmazonMwsFeeds();
    $xml = $service->FormatFeed($mapping->getTransposed(), INVENTORY_FEED);
    $tmp_xml_file = file_directory_temp() . "/mws_inventory" . time();

    if ($file = file_unmanaged_save_data($xml, $tmp_xml_file, FILE_EXISTS_RENAME)) {
      $openfile = fopen($file, 'r');
      $service->SubmitFeed($openfile, INVENTORY_FEED, $service->marketplace_id());

      fclose($openfile);
      file_unmanaged_delete($file);
    }
    else {
      watchdog("CommerceAmazonMWS", "%function: unable to write feed to temporary directory; check file permissions",
        array("%function" => __FUNCTION__), WATCHDOG_ERROR);
    }
  }
}

/**
 * Transposes multiple items by nid and submit them as a pricing feed.
 *
 * @param array $items
 */
function _commerce_amazon_mws_task_update_multiple_item_prices($items = array()) {

  module_load_include('inc', 'commerce_amazon_mws', 'includes/commerce_amazon_mws.feed_types');
  $products = array();

  // Get transposed entries for each variation.
  foreach ($items as $nid) {
    if ($mapping = new CommerceAmazonMWSTypeMapping($nid, "Price")) {
      foreach ($mapping->getTransposed() as $variation) {
        $products[] = $variation;
      }
    }
  }

  // Convert all of the transposed products to an XML pricing feed and submit it.
  if (count($products)) {
    $service = new CommerceAmazonMwsFeeds();
    $xml = $service->FormatFeed($products, PRICING_FEED);
    $tmp_xml_file = file_directory_temp() . "/mws_prices" . time();

    if ($file = file_unmanaged_save_data($xml, $tmp_xml_file, FILE_EXISTS_RENAME)) {
      $openfile = fopen($file, 'r');
      $service->SubmitFeed($openfile, PRICING_FEED, $service->marketplace_id());

      fclose($openfile);
      file_unmanaged_delete($file);
    }
    else {
      watchdog("CommerceAmazonMWS", "%function: unable to write feed to temporary directory; check file permissions",
        array("%function" => __FUNCTION__), WATCHDOG_ERROR);
    }
  }
}

/**
 * Maps, transposes and submits and product price change feed
 * for an item.
 *
 * @param $item - a product display node
 */
function _commerce_amazon_mws_task_update_item_price($item) {

  if ($mapping = new CommerceAmazonMWSTypeMapping($item->nid, "Price")) {
    module_load_include('inc', 'commerce_amazon_mws', 'includes/commerce_amazon_mws.feed_types');

    $service = new CommerceAmazonMwsFeeds();
    $xml = $service->FormatFeed($mapping->getTransposed(), PRICING_FEED);
    $tmp_xml_file = file_directory_temp() . "/mws_prices" . time();

    if ($file = file_unmanaged_save_data($xml, $tmp_xml_file, FILE_EXISTS_RENAME)) {
      $openfile = fopen($file, 'r');
      $service->SubmitFeed($openfile, PRICING_FEED, $service->marketplace_id());

      fclose($openfile);
      file_unmanaged_delete($file);
    }
    else {
      watchdog("CommerceAmazonMWS", "%function: unable to write feed to temporary directory; check file permissions",
        array("%function" => __FUNCTION__), WATCHDOG_ERROR);
    }
  }
}

/**
 * Gets the 'delete' cache from the Amazon MWS Cache
 * and submits a product feed with the Delete operation.
 */
function commerce_amazon_mws_task_delete_remote_items() {
  module_load_include('inc', 'commerce_amazon_mws', 'includes/commerce_amazon_mws.feed_types');
  $service = new CommerceAmazonMwsFeeds();

  // Only run if cron is enabled for host.
  if (!$service->cron_enabled() || !$service->host_enabled()) {
    return;
  }

  if ($cache = CommerceAmazonMWSCache::getCache('delete', 'delete')) {
    $products = array();
    foreach ($cache as $nid) {
      // Check to make sure the node still exists.
      $mapping = new CommerceAmazonMWSTypeMapping($nid, 'ProductDelete');
      if ($mapping->display_node) {
        foreach ($mapping->getTransposed(TRUE) as $transposed) {
          $products[] = $transposed;
        }
      }
    }

    $xml = $service->FormatFeed($products, PRODUCT_FEED, 'Delete');
    $tmp_xml_file = file_directory_temp() . "/mws_product_delete" . time();

    if ($file = file_unmanaged_save_data($xml, $tmp_xml_file, FILE_EXISTS_RENAME)) {
      $openfile = fopen($file, 'r');
      $service->SubmitFeed($openfile, PRODUCT_FEED, $service->marketplace_id());

      fclose($openfile);
      file_unmanaged_delete($file);

      // Delete all the items in the 'delete' cache.
      CommerceAmazonMWSCache::deleteCache('delete', 'delete');
    }
    else {
      watchdog("CommerceAmazonMWS", "%function: unable to write feed to temporary directory; check file permissions",
        array("%function" => __FUNCTION__), WATCHDOG_ERROR);
    }
  }
}

/**
 * Updates the stock quantities of local items from a
 * Flat File Orders Data report, this won't work with
 * other reports unless they match the same data structure
 * and have a 'sku' and 'quantity-purchased' field.
 *
 * @param $item - a product display node
 *
 * @return bool
 */
function _commerce_amazon_mws_task_update_local_item_stock($item) {
  // @TODO need to handle alternate SKU items here
  // Get the product and update its stock directly from the report quantity.
  if ($product = commerce_product_load_by_sku($item['sku'])) {

    $quantity_field_name = _commerce_amazon_mws_helper_get_variation_quantity_field_name($product);

    // Make sure we have a valid quantity field name, and that the item is
    // listed on Amazon.
    if ($quantity_field_name) {
      $wrapper = entity_metadata_wrapper('commerce_product', $product);
      if (isset($wrapper->{$quantity_field_name})) {
        // The new quantity should be either 0 or the current quantity - the
        // amount purchased.
        $current_quantity = $wrapper->{$quantity_field_name}->value();
        $new_quantity = ($current_quantity - (float) $item['quantity']);

        $wrapper->{$quantity_field_name}->set($new_quantity);
        $wrapper->save();

        watchdog('CommerceAmazonMWS_stock_adjustment', 'item %item quantity set from %current to %new', array(
          '%item' => $item['sku'],
          '%new' => $new_quantity,
          '%current' => $current_quantity,
        ), WATCHDOG_INFO);

        return TRUE;
      }
    }

    // Log a warning if the quantity field can't be found for the product.
    $logging = new CommerceAmazonMWSLogging();
    $logging->warning("%function returned 'valid Quantity field not found in product %product' as error", array(
      "%function" => __FUNCTION__,
      "%product" => $product->sku,
    ));
  }

  watchdog('CommerceAmazonMWS_stock_adjustment', 'product could not load from SKU %sku',
    array('%sku' => $item['sku']), WATCHDOG_WARNING);

  return FALSE;
}

/**
 * Gets a report.
 *
 * @param $report_id
 *
 * @return array|null
 *
 * @throws Exception
 */
function _commerce_amazon_mws_get_report($report_id) {
  $report = NULL;

  if (!$report = CommerceAmazonMWSCache::getCache($report_id, COMMERCE_AMAZONMWS_REPORT)) {
    $report_service = new CommerceAmazonMwsReports();
    if ($report = $report_service->GetReport($report_id)) {
      CommerceAmazonMWSCache::setCache($report_id, COMMERCE_AMAZONMWS_REPORT, $report);
    }
  }

  return $report;
}

/**
 * Gets a quantity field for a variation from the json file when a full mapping
 * class instance isn't needed.
 *
 * @param $product
 *
 * @return null|string
 */
function _commerce_amazon_mws_helper_get_variation_quantity_field_name($product) {
  $path = drupal_get_path('module', 'commerce_amazon_mws') . '/includes/commerce_amazon_mws.map.json';

  if ($mapping = json_decode(file_get_contents($path), TRUE)) {

    // @Todo don't let this grab the first one, for now mapping only handles one display type per product
    if (isset($mapping['product_types'][$product->type]['display_nodes'][0])) {
      $display_node = $mapping['product_types'][$product->type]['display_nodes'][0];

      // The quantity field should be part of the Inventory feed, for updating stock quantities.
      if (isset($mapping['display_node'][$display_node]['Inventory']['fields']['Quantity'])) {
        $quantity_field = $mapping['display_node'][$display_node]['Inventory']['fields']['Quantity'];

        // Match variation fields and return the quantity field name.
        preg_match('/\$(.+)\$/', $quantity_field, $matches);
        if (isset($matches[1])) {
          return $matches[1];
        }
      }
    }
  }

  return NULL;
}

/**
 * Gets the list_flag field value from a product and feed type.
 *
 * @param $product
 * @param $feed_type
 *
 * @return bool
 *   returns a boolean for true or false, or -1 for missing field
 */
function _commerce_amazon_mws_helper_get_list_flag_field_value($product, $feed_type) {
  $path = drupal_get_path('module', 'commerce_amazon_mws') . '/includes/commerce_amazon_mws.map.json';

  if ($mapping = json_decode(file_get_contents($path), TRUE)) {

    if (isset($mapping['product_types'][$product->type]['display_nodes'][0])) {
      // @Todo don't just make this grab the first one; for now mapping only handles one display type per product
      $display_node = $mapping['product_types'][$product->type]['display_nodes'][0];

      if (isset($mapping['display_node'][$display_node][$feed_type]['list_flag'])) {
        $list_flag_field = $mapping['display_node'][$display_node][$feed_type]['list_flag'];

        if (!$list_flag_field) {
          return -1;
        }

        $product_field = $mapping['display_node'][$display_node][$feed_type]['variation'];

        // Match variation fields and get the list flag field name.
        preg_match('/\%(.+)\%/', $list_flag_field, $matches);
        if (isset($matches[1])) {
          $list_flag_field = $matches[1];

          if ($nid = _commerce_amazon_mws_helper_get_display_node_nid_from_product($product_field, $product)) {
            $wrapper = entity_metadata_wrapper('node', $nid);

            if (isset($wrapper->{$list_flag_field})) {
              return $wrapper->{$list_flag_field}->value();
            }
          }
        }
      }
    }
  }

  return -1;
}

/**
 * Returns an nid for a display node related to a product variation.
 *
 * @param $product_field
 * @param $product
 *
 * @return mixed|null
 */
function _commerce_amazon_mws_helper_get_display_node_nid_from_product($product_field, $product) {

  $query = new EntityFieldQuery();
  $result = $query->entityCondition('entity_type', 'node', '=')
    ->fieldCondition($product_field, 'product_id', $product->product_id, '=')
    ->range(0, 1)
    ->execute();

  if ($result) {
    $result = array_shift($result);
    return key($result);
  }

  return NULL;
}
