<?php

/**
 * @file commerce_amazon_mws.feed_types.inc
 * @description
 *  constants for the feed types listed here:
 *  http://docs.developer.amazonservices.com/en_CA/feeds/Feeds_FeedType.html
 */

define('PRODUCT_FEED', '_POST_PRODUCT_DATA_');
define('INVENTORY_FEED', '_POST_INVENTORY_AVAILABILITY_DATA_');
define('OVERRIDES_FEED', '_POST_PRODUCT_OVERRIDES_DATA_');
define('PRICING_FEED', '_POST_PRODUCT_PRICING_DATA_');
define('PRODUCT_IMAGES_FEED', '_POST_PRODUCT_IMAGE_DATA_');
define('RELATIONSHIPS_FEED', '_POST_PRODUCT_RELATIONSHIP_DATA_');
define('FLAT_FILE_INVENTORY_LOADER_FEED', '_POST_FLAT_FILE_INVLOADER_DATA_');
define('FLAT_FILE_LISTINGS_FEED', '_POST_FLAT_FILE_LISTINGS_DATA_');
define('FLAT_FILE_BOOK_LOADER_FEED', '_POST_FLAT_FILE_BOOKLOADER_DATA_');
define('FLAT_FILE_MUSIC_LOADER_FEED', '_POST_FLAT_FILE_CONVERGENCE_LISTINGS_DATA_');
define('FLAT_FILE_VIDEO_LOADER_FEED', '_POST_FLAT_FILE_LISTINGS_DATA_');
define('FLAT_FILE_PRICE_AND_QUANTITY_UPDATE_FEED', '_POST_FLAT_FILE_PRICEANDQUANTITYONLY_UPDATE_DATA_');
define('UIEE_INVENTORY_FEED', '_POST_UIEE_BOOKLOADER_DATA_');
define('ACES_3_0_DATA_FEED', '_POST_STD_ACES_DATA_');
