<?php
/**
 * @file commerce_amazon_mws.Service.php
 */

/**
 * Uses the official PHP library of CommerceAmazonMWSService, please check
 * the README.txt file on instructions how to install, and the requires
 * file structure. This pools together functions from different services, along
 * with error handling and debugging.
 *
 */
class CommerceAmazonMWSService {

  protected $logging;
  protected $service;
  protected $library_path;
  protected $libraries;
  public static $included;

  /**
   * @internal param $service_class_name
   */
  function __construct() {
    $this->libraries = array();
    $this->library_path = '';

    $this->init();
    // Static included array so we can keep track of
    // loaded libraries through multiple instances.
    if (!self::$included) {
      self::$included = array();
    }

    $this->logging = new CommerceAmazonMWSLogging();
  }

  // Set $this->library path in child instances and then load libraries;
  // as each instance will have it's own library; check the init function
  // of commerce_amazon_mws.Feeds.php, commerce_amazon_mws.Reports.php
  // for example.
  protected function init() {}

  /**
   * Spawns an Amazon MWS class.
   *
   * @param $service_name
   * @param array $args
   *
   * @return null|Object
   */
  protected function spawnService($service_name, $args = array()) {
    try {
      $class = new ReflectionClass($service_name);

      return (!empty($args)) ? $class->newInstanceArgs($args) : $class->newInstance();
    }
    catch (Exception $e) {
      $this->logging->error("spawnService returned '%exception' as error when trying to spawn %service_name.",
        array("%exception" => $e->getMessage(), "%service_name" => $service_name));
      return NULL;
    }
  }

  /**
   * Gets AWS Access Key ID.
   *
   * @return string
   */
  public function access_key_id() {
    return variable_get('commerce_amazon_mws_prod_access-key-id', '');
  }

  /**
   * Gets Secret Access Key.
   *
   * @return string
   *
   */
  public function secret_access_key() {
    return variable_get('commerce_amazon_mws_prod_secret-access-key', '');
  }

  /**
   * Gets Application Name.
   *
   * @return string
   */
  public function application_name() {
    return variable_get('commerce_amazon_mws_prod_application-name', '');
  }

  /**
   * Returns boolean for whether or not Amazonr MWS cron is enabled.
   *
   * @return bool
   */
  public function cron_enabled() {
    return variable_get('commerce_amazon_mws_enable_cron', 0);
  }

  /**
   * Returns boolean for whether or host cron is enabled.
   *
   * @return bool
   */
  public function host_enabled() {
    return variable_get('commerce_amazon_mws_host_restrict', '') == gethostname();
  }

  /**
   * Gets Version Number.
   *
   * @return string
   */
  public function version_number() {
    return variable_get('commerce_amazon_mws_prod_version-number', '');
  }

  /**
   * Gets Marketplace ID.
   *
   * @return string
   */
  public function marketplace_id() {
    return variable_get('commerce_amazon_mws_prod_marketplace-id', '');
  }

  /**
   * Gets Merchant Token.
   *
   * @return string
   */
  public function merchant_token() {
    return variable_get('commerce_amazon_mws_prod_merchant-token', '');
  }

  /**
   * Gets Seller ID.
   *
   * @return string
   */
  public function seller_id() {
    return variable_get('commerce_amazon_mws_prod_seller-id', '');
  }

  /**
   * Gets Auth Token.
   *
   * @return string
   *
   */
  public function auth_token() {
    return variable_get('commerce_amazon_mws_prod_auth-token', '');
  }

  /**
   * Loads in a library file, includes it without adding the .php library extension.
   * This function is mainly used to avoid loading in all libraries. If a library
   * is already included, it won't reinclude it.
   *
   * @param string $library_name
   */
  protected function useLibrary($library_name = '') {
    if (!in_array($library_name, self::$included) && isset($this->libraries[$library_name])) {
      try {
        include($this->libraries[$library_name]);

        self::$included[] = $library_name;
      }
      catch (Exception $e) {
        $this->logging->error("%s library does not exist", array("%s" => $library_name));
      }
    }
  }

  /**
   * Creates a list of all the files that can be included as a library.
   */
  protected function loadLibraries() {
    $this->libraries = array();
    $directories = array("Model", "Samples", '');

    foreach($directories as $directory) {
      foreach(scandir($this->library_path.DIRECTORY_SEPARATOR.$directory) as $file) {
        if ($file != "." && $file != "..") {
          //this is a pretty weird ternary usage, would be better as a traditional if/else statement
          (empty($directory))
            ? $fullpath = $this->library_path.DIRECTORY_SEPARATOR . $file
            : $fullpath = $this->library_path.DIRECTORY_SEPARATOR . $directory . DIRECTORY_SEPARATOR . $file;

          $pathinfo = pathinfo($fullpath);
          if (isset($pathinfo['extension']) && $pathinfo['extension'] == "php") {
            $this->libraries[$pathinfo['filename']] = $fullpath;
          }
        }
      }
    }
  }

  /**
   * Returns the default configuration for a service; serviceUrl is required.
   *
   * @param $serviceUrl
   *
   * @return array
   */
  protected function defaultConfig($serviceUrl = '') {
    return array (
      'ServiceURL' => $serviceUrl,
      'ProxyHost' => null,
      'ProxyPort' => -1,
      'ProxyUsername' => null,
      'ProxyPassword' => null,
      'MaxErrorRetry' => 3,
    );
  }

  /**
   * Processes XML from a service call response.
   *
   * @param $response
   *
   * @return null|DOMDocument
   */
  protected function processResponse($response) {
    try {
      $data = simplexml_load_string($response->toXML());

      return json_decode(json_encode((array)$data), TRUE);
    }
    catch (Exception $e) {
      $this->logging->error("processResponse returned '%exception' as error when trying to process response.",
        array("%exception" => $e->getMessage()));
      return NULL;
    }
  }

}
