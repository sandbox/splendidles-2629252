<?php

/**
 * @file commerce_amazon_mws.Reports.php
 */

/**
 * Class CommerceAmazonMwsReports.
 *
 * Extends the core service and defines the operations
 * for gathering and submitting reports.
 */
class CommerceAmazonMwsReports extends CommerceAmazonMWSService {

  protected $libraries;

  /**
   * @inheritdoc
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   * @inheritdoc
   */
  protected function init() {
    $this->library_path = $_SERVER['DOCUMENT_ROOT'] . '/' . drupal_get_path('module', 'commerce_amazon_mws')
      . '/libraries/nonapod/AmazonMWS/MarketplaceWebServiceReports';
    $this->loadLibraries();
  }

  /**
   * docs.developer.amazonservices.com/en_US/reports/Reports_CancelReportRequests.html.
   *
   * @param string $reportRequestIdList
   * @param string $reportTypeList
   * @param string $reportProcessingStatusList
   * @param datetime $requestedFromDate
   * @param datetime $requestedToDate
   *
   * @return array|null
   */
  public function CancelReportRequests($reportRequestIdList = NULL,
  $reportTypeList = NULL,
  $reportProcessingStatusList = NULL,
                                $requestedFromDate = NULL,
  $requestedToDate = NULL) {
    $this->useLibrary("Client");
    $this->useLibrary("CancelReportRequestsRequest");

    $client = $this->spawnService("MarketplaceWebService_Client", array($this->access_key_id(),
      $this->secret_access_key(), $this->config(), $this->application_name(), $this->version_number(),
    ));
    $request = $this->spawnService("MarketplaceWebService_Model_CancelReportRequestsRequest");

    if ($client && $request) {
      try {

        $request->setMerchant($this->seller_id());
        $request->setMWSAuthToken($this->auth_token());

        if ($reportRequestIdList) {
          $id_list = $this->spawnService("MarketplaceWebService_Model_IdList");
          $id_list->setId($reportRequestIdList);
          $this->ReportRequestIdList = $id_list;
        }

        if ($reportTypeList) {
          $type_list = $this->spawnService("MarketplaceWebService_Model_TypeList");
          $type_list->setType($reportTypeList);
          $this->ReportTypeList = $type_list;
        }

        if ($reportProcessingStatusList) {
          $status_list = $this->spawnService("MarketplaceWebService_Model_StatusList");
          $status_list->setStatus($reportProcessingStatusList);
          $this->ReportProcessingStatusList = $status_list;
        }

        if ($requestedFromDate) {
          $this->RequestedFromDate = $requestedFromDate;
        }

        if ($requestedToDate) {
          $this->RequestedToDate = $requestedToDate;
        }

        $response = $client->CancelReportRequests($request);

        return $this->processResponse($response);
      }
      catch (Exception $e) {
        $this->logging->error("%function returned '%exception' as error", array(
          "%function" => __FUNCTION__,
          "%exception" => $e->getMessage(),
        ));
      }
    }

    return NULL;
  }

  /**
   * http://docs.developer.amazonservices.com/en_US/reports/Reports_RequestReport.html.
   *
   * @param string $reportType
   * @param datetime $startDate
   * @param datetime $endDate
   * @param string $reportOptions
   * @param string $marketPlaceIdList
   *
   * @return array|null
   */
  public function RequestReport($reportType,
  $startDate = NULL,
  $endDate = NULL,
  $reportOptions = NULL,
                         $marketPlaceIdList = NULL) {
    $this->useLibrary("Client");
    $this->useLibrary("RequestReportRequest");

    $client = $this->spawnService("MarketplaceWebService_Client", array($this->access_key_id(),
      $this->secret_access_key(), $this->config(), $this->application_name(), $this->version_number(),
    ));
    $request = $this->spawnService("MarketplaceWebService_Model_RequestReportRequest");

    if ($client && $request) {
      try {

        $request->setMerchant($this->seller_id());
        $request->setMWSAuthToken($this->auth_token());
        $request->ReportType = $reportType;

        if ($startDate) {
          $request->StartDate = $startDate;
        }

        if ($endDate) {
          $request->EndDate = $endDate;
        }

        if ($reportOptions) {
          $request->ReportOptions = $reportOptions;
        }

        $response = $client->RequestReport($request);

        return $this->processResponse($response);
      }
      catch (Exception $e) {
        $this->logging->error("%function returned '%exception' as error", array(
          "%function" => __FUNCTION__,
          "%exception" => $e->getMessage(),
        ));
      }
    }

    return NULL;
  }

  /**
   * http://docs.developer.amazonservices.com/en_US/reports/Reports_GetReport.html.
   *
   * @param string $reportId
   *
   * @return array|null
   */
  public function GetReport($reportId) {
    $this->useLibrary("Client");
    $this->useLibrary("GetReportRequest");
    $client = $this->spawnService("MarketplaceWebService_Client", array($this->access_key_id(),
      $this->secret_access_key(), $this->config(), $this->application_name(), $this->version_number(),
    ));
    $request = $this->spawnService("MarketplaceWebService_Model_GetReportRequest");

    if ($client && $request) {
      try {

        $request->setMerchant($this->seller_id());
        $request->setMWSAuthToken($this->auth_token());
        $request->setReport(@fopen('php://memory', 'rw+'));

        $request->ReportId = $reportId;
        $client->GetReport($request);

        // The request holds the report response.
        return $this->processReport($request);
      }
      catch (Exception $e) {
        $this->logging->error("%function returned '%exception' as error", array(
          "%function" => __FUNCTION__,
          "%exception" => $e->getMessage(),
        ));
      }
    }

    return NULL;
  }

  /**
   * http://docs.developer.amazonservices.com/en_US/reports/Reports_GetReportCount.html.
   *
   * @param string $reportTypeList
   * @param string $acknowledged
   * @param string $availableFromDate
   * @param string $availableToDate
   *
   * @return array|null
   */
  public function GetReportCount($reportTypeList = NULL,
  $acknowledged = NULL,
  $availableFromDate = NULL,
                          $availableToDate = NULL) {
    $this->useLibrary("Client");
    $this->useLibrary("GetReportCountRequest");

    $client = $this->spawnService("MarketplaceWebService_Client", array($this->access_key_id(),
      $this->secret_access_key(), $this->config(), $this->application_name(), $this->version_number(),
    ));
    $request = $this->spawnService("MarketplaceWebService_Model_GetReportCountRequest");

    if ($client && $request) {
      try {

        $request->setMerchant($this->seller_id());
        $request->setMWSAuthToken($this->auth_token());

        if ($reportTypeList) {
          $type_list = $this->spawnService("MarketplaceWebService_Model_TypeList");
          $type_list->setType($reportTypeList);
          $this->ReportTypeList = $type_list;
        }

        if ($acknowledged) {
          $request->Acknowledged = $acknowledged;
        }

        if ($availableFromDate) {
          $request->AvailableFromDate = $availableFromDate;
        }

        if ($availableToDate) {
          $request->AvailableToDate = $availableToDate;
        }

        $response = $client->GetReportCount($request);

        return $this->processResponse($response);
      }
      catch (Exception $e) {
        $this->logging->error("%function returned '%exception' as error", array(
          "%function" => __FUNCTION__,
          "%exception" => $e->getMessage(),
        ));
      }
    }

    return NULL;
  }

  /**
   * http://docs.developer.amazonservices.com/en_US/reports/Reports_GetReportList.html.
   *
   * @param string $maxCount
   * @param string $reportTypeList
   * @param string $acknowledged
   * @param datetime $availableFromDate
   * @param datetime $availableToDate
   * @param string $reportRequestIdList
   *
   * @return array|null
   */
  public function GetReportList($maxCount = NULL,
  $reportTypeList = NULL,
  $acknowledged = NULL,
  $availableFromDate = NULL,
                         $availableToDate = NULL,
  $reportRequestIdList = NULL) {
    $this->useLibrary("Client");
    $this->useLibrary("GetReportListRequest");

    $client = $this->spawnService("MarketplaceWebService_Client", array($this->access_key_id(),
      $this->secret_access_key(), $this->config(), $this->application_name(), $this->version_number(),
    ));
    $request = $this->spawnService("MarketplaceWebService_Model_GetReportListRequest");

    if ($client && $request) {
      try {

        $request->setMerchant($this->seller_id());
        $request->setMWSAuthToken($this->auth_token());

        if ($maxCount) {
          $request->MaxCount = $maxCount;
        }

        if ($reportTypeList) {
          $type_list = $this->spawnService("MarketplaceWebService_Model_TypeList");
          $type_list->setType($reportTypeList);
          $this->ReportTypeList = $type_list;
        }

        if ($acknowledged) {
          $request->Acknowledged = $acknowledged;
        }

        if ($availableFromDate) {
          $request->AvailableFromDate = $availableFromDate;
        }

        if ($availableToDate) {
          $request->AvailableToDate = $availableToDate;
        }

        if ($reportRequestIdList) {
          $request->ReportRequestIdList = $reportRequestIdList;
        }

        $response = $client->GetReportList($request);

        return $this->processResponse($response);
      }
      catch (Exception $e) {
        $this->logging->error("%function returned '%exception' as error", array(
          "%function" => __FUNCTION__,
          "%exception" => $e->getMessage(),
        ));
      }
    }

    return NULL;
  }

  /**
   * http://docs.developer.amazonservices.com/en_US/reports/Reports_GetReportListByNextToken.html.
   *
   * @param string $nextToken
   *
   * @return array|null
   */
  public function GetReportListByNextToken($nextToken) {
    $this->useLibrary("Client");
    $this->useLibrary("GetReportListByNextTokenRequest");

    $client = $this->spawnService("MarketplaceWebService_Client", array($this->access_key_id(),
      $this->secret_access_key(), $this->config(), $this->application_name(), $this->version_number(),
    ));
    $request = $this->spawnService("MarketplaceWebService_Model_GetReportListByNextTokenRequest");

    if ($client && $request) {
      try {

        $request->setMerchant($this->seller_id());
        $request->setMWSAuthToken($this->auth_token());

        if ($nextToken) {
          $request->NextToken = $nextToken;
        }

        $response = $client->GetReportListByNextToken($request);
        return $this->processResponse($response);
      }
      catch (Exception $e) {
        $this->logging->error("%function returned '%exception' as error", array(
          "%function" => __FUNCTION__,
          "%exception" => $e->getMessage(),
        ));
      }
    }

    return NULL;
  }

  /**
   * http://docs.developer.amazonservices.com/en_US/reports/Reports_GetReportRequestCount.html.
   *
   * @param string $reportTypeList
   * @param string $reportProcessingStatusList
   * @param datetime $requestedFromDate
   * @param datetime $requestedToDate
   *
   * @return array|null
   */
  public function GetReportRequestCount($reportTypeList = NULL,
  $reportProcessingStatusList = NULL,
  $requestedFromDate = NULL,
                                 $requestedToDate = NULL) {
    $this->useLibrary("Client");
    $this->useLibrary("GetReportRequestCountRequest");

    $client = $this->spawnService("MarketplaceWebService_Client", array($this->access_key_id(),
      $this->secret_access_key(), $this->config(), $this->application_name(), $this->version_number(),
    ));
    $request = $this->spawnService("MarketplaceWebService_Model_GetReportRequestCountRequest");

    if ($client && $request) {
      try {

        $request->setMerchant($this->seller_id());
        $request->setMWSAuthToken($this->auth_token());

        if ($reportTypeList) {
          $type_list = $this->spawnService("MarketplaceWebService_Model_TypeList");
          $type_list->setType($reportTypeList);
          $this->ReportTypeList = $type_list;
        }

        if ($reportProcessingStatusList) {
          $status_list = $this->spawnService("MarketplaceWebService_Model_StatusList");
          $status_list->setStatus($reportProcessingStatusList);
          $this->ReportProcessingStatusList = $status_list;
        }

        if ($requestedFromDate) {
          $this->RequestedFromDate = $requestedFromDate;
        }

        if ($requestedToDate) {
          $this->RequestedToDate = $requestedToDate;
        }

        $response = $client->GetReportRequestCount($request);
        return $this->processResponse($response);
      }
      catch (Exception $e) {
        $this->logging->error("%function returned '%exception' as error", array(
          "%function" => __FUNCTION__,
          "%exception" => $e->getMessage(),
        ));
      }
    }

    return NULL;
  }

  /**
   * http://docs.developer.amazonservices.com/en_US/reports/Reports_GetReportRequestList.html.
   *
   * @param string $reportRequestIdList
   * @param string $reportTypeList
   * @param string $reportProcessingStatusList
   * @param int $maxCount
   * @param dateTime $requestedFromDate
   * @param dateTime $requestedToDate
   *
   * @return array|null
   */
  public function GetReportRequestList($reportRequestIdList = NULL,
  $reportTypeList = NULL,
  $reportProcessingStatusList = NULL,
                                $maxCount = NULL,
  $requestedFromDate = NULL,
  $requestedToDate = NULL) {
    $this->useLibrary("Client");
    $this->useLibrary("GetReportRequestListRequest");
    $this->useLibrary("IdList");

    $client = $this->spawnService("MarketplaceWebService_Client", array($this->access_key_id(),
      $this->secret_access_key(), $this->config(), $this->application_name(), $this->version_number(),
    ));
    $request = $this->spawnService("MarketplaceWebService_Model_GetReportRequestListRequest");

    if ($client && $request) {
      try {

        $request->setMerchant($this->seller_id());
        $request->setMWSAuthToken($this->auth_token());

        if ($reportRequestIdList) {
          $id_list = $this->spawnService("MarketplaceWebService_Model_IdList");
          $id_list->setId($reportRequestIdList);
          $request->ReportRequestIdList = $id_list;
        }

        if ($reportTypeList) {
          $type_list = $this->spawnService("MarketplaceWebService_Model_TypeList");
          $type_list->setType($reportTypeList);
          $this->ReportTypeList = $type_list;
        }

        if ($reportProcessingStatusList) {
          $status_list = $this->spawnService("MarketplaceWebService_Model_StatusList");
          $status_list->setStatus($reportProcessingStatusList);
          $this->ReportProcessingStatusList = $status_list;
        }

        if ($maxCount) {
          $request->MaxCount = $maxCount;
        }

        if ($requestedFromDate) {
          $request->RequestedFromDate = $requestedFromDate;
        }

        if ($requestedToDate) {
          $request->RequestedToDate = $requestedToDate;
        }

        $response = $client->GetReportRequestList($request);

        return $this->processResponse($response);
      }
      catch (Exception $e) {
        $this->logging->error("%function returned '%exception' as error", array(
          "%function" => __FUNCTION__,
          "%exception" => $e->getMessage(),
        ));
      }
    }

    return NULL;
  }

  /**
   * http://docs.developer.amazonservices.com/en_US/reports/Reports_GetReportRequestListByNextToken.html.
   *
   * @param string $nextToken
   *
   * @return array|null
   */
  public function GetReportRequestListByNextToken($nextToken) {
    $this->useLibrary("Client");
    $this->useLibrary("GetReportListByNextTokenRequest");

    $client = $this->spawnService("MarketplaceWebService_Client", array($this->access_key_id(),
      $this->secret_access_key(), $this->config(), $this->application_name(), $this->version_number(),
    ));
    $request = $this->spawnService("MarketplaceWebService_Model_GetReportListByNextTokenRequest");

    if ($client && $request) {
      try {
        $request->setMerchant($this->seller_id());
        $request->setMWSAuthToken($this->auth_token());

        $request->NextToken = $nextToken;

        $response = $client->GetReportListByNextToken($request);

        return $this->processResponse($response);
      }
      catch (Exception $e) {
        $this->logging->error("%function returned '%exception' as error", array(
          "%function" => __FUNCTION__,
          "%exception" => $e->getMessage(),
        ));
      }
    }

    return NULL;
  }

  /**
   * http://docs.developer.amazonservices.com/en_CA/reports/Reports_GetReportScheduleCount.html.
   *
   * @param array $reportTypeList
   *
   * @return array|null
   */
  public function GetReportScheduleCount($reportTypeList) {
    $this->useLibrary("Client");
    $this->useLibrary("GetReportScheduleCountRequest");

    $client = $this->spawnService("MarketplaceWebService_Client", array($this->access_key_id(),
      $this->secret_access_key(), $this->config(), $this->application_name(), $this->version_number(),
    ));
    $request = $this->spawnService("MarketplaceWebService_Model_GetReportScheduleCountRequest");

    if ($client && $request) {
      try {

        $request->setMerchant($this->seller_id());
        $request->setMWSAuthToken($this->auth_token());
        $request->setReport(@fopen('php://memory', 'rw+'));

        if ($reportTypeList) {
          $type_list = $this->spawnService("MarketplaceWebService_Model_TypeList");
          $type_list->setType($reportTypeList);
          $request->ReportTypeList = $type_list;
        }

        $response = $client->GetReportScheduleCount($request);

        return $this->processResponse($response);
      }
      catch (Exception $e) {
        $this->logging->error("%function returned '%exception' as error", array(
          "%function" => __FUNCTION__,
          "%exception" => $e->getMessage(),
        ));
      }
    }

    return NULL;
  }

  /**
   * http://docs.developer.amazonservices.com/en_CA/reports/Reports_GetReportScheduleList.html.
   *
   * @param array $reportTypeList
   *
   * @return array|null
   */
  public function GetReportScheduleList($reportTypeList) {
    $this->useLibrary("Client");
    $this->useLibrary("GetReportScheduleListRequest");

    $client = $this->spawnService("MarketplaceWebService_Client", array($this->access_key_id(),
      $this->secret_access_key(), $this->config(), $this->application_name(), $this->version_number(),
    ));
    $request = $this->spawnService("MarketplaceWebService_Model_GetReportScheduleListRequest");

    if ($client && $request) {
      try {

        $request->setMerchant($this->seller_id());
        $request->setMWSAuthToken($this->auth_token());
        $request->setReport(@fopen('php://memory', 'rw+'));

        if ($reportTypeList) {
          $type_list = $this->spawnService("MarketplaceWebService_Model_TypeList");
          $type_list->setType($reportTypeList);
          $request->ReportTypeList = $type_list;
        }

        $response = $client->GetReportScheduleList($request);

        return $this->processResponse($response);
      }
      catch (Exception $e) {
        $this->logging->error("%function returned '%exception' as error", array(
          "%function" => __FUNCTION__,
          "%exception" => $e->getMessage(),
        ));
      }
    }

    return NULL;
  }

  /**
   * http://docs.developer.amazonservices.com/en_CA/reports/Reports_ManageReportSchedule.html.
   *
   * @param string $reportType
   * @param string $schedule
   * @param datetime $scheduleDate
   *
   * @return array|null
   */
  public function ManageReportSchedule($reportType = NULL, $schedule = NULL, $scheduleDate = NULL) {
    $this->useLibrary("Client");
    $this->useLibrary("ManageReportScheduleRequest");

    $client = $this->spawnService("MarketplaceWebService_Client", array($this->access_key_id(),
      $this->secret_access_key(), $this->config(), $this->application_name(), $this->version_number(),
    ));
    $request = $this->spawnService("MarketplaceWebService_Model_ManageReportScheduleRequest");

    if ($client && $request) {
      try {

        if ($reportType) {
          $request->setReportType($reportType);
        }

        if ($schedule) {
          $request->setSchedule($schedule);
        }

        if ($scheduleDate) {
          $request->setScheduleDate($scheduleDate);
        }

        $request->setMerchant($this->seller_id());
        $request->setMWSAuthToken($this->auth_token());
        $request->setReport(@fopen('php://memory', 'rw+'));

        $response = $client->ManageReportSchedule($request);

        return $this->processResponse($response);
      }
      catch (Exception $e) {
        $this->logging->error("%function returned '%exception' as error", array(
          "%function" => __FUNCTION__,
          "%exception" => $e->getMessage(),
        ));
      }
    }

    return NULL;
  }

  /**
   * Processes a report from a request once the request has been made.
   * Returns an array from report CSV.
   *
   * @param $request
   * @param $delimiter
   *
   * @return array
   */
  public function processReport($request, $delimiter = "\t") {
    $report = array();

    try {
      $header = stream_get_line($request->getReport(), 1024, "\n");
      $rows = explode("\n", stream_get_contents($request->getReport()));

      foreach ($rows as $row) {
        // Rtrim the header so the last item doesn't have a space at the end.
        $report[] = array_combine(explode($delimiter, rtrim($header)), explode($delimiter, $row));
      }

      return $report;
    }
    catch (Exception $e) {
      $this->logging->error("%function returned '%exception' as error", array(
        "%function" => __FUNCTION__,
        "%exception" => $e->getMessage(),
      ));
    }

    return NULL;
  }

  /**
   * Returns the config required to setup the product client.
   *
   * @return array
   */
  protected function config() {
    return $this->defaultConfig("https://mws.amazonservices.com/");
  }

}
