<?php

/**
 * @file commerce_amazon_mws.Feeds.php
 */

/**
 * Class CommerceAmazonMwsFeeds.
 */
class CommerceAmazonMwsFeeds extends CommerceAmazonMWSService {

  protected $libraries;

  /**
   * @inheritdoc
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   * @inheritdoc
   */
  protected function init() {
    $this->library_path = $_SERVER['DOCUMENT_ROOT'] . '/' . drupal_get_path('module', 'commerce_amazon_mws')
      . '/libraries/nonapod/AmazonMWS/MarketplaceWebServiceFeeds';
    $this->loadLibraries();
  }

  /**
   * http://docs.developer.amazonservices.com/en_CA/feeds/Feeds_CancelFeedSubmissions.html.
   *
   * @param string $feedSubmissionIdList
   * @param string $feedTypeList
   * @param dateTime $submittedFromDate
   * @param dateTime $submittedToDate
   *
   * @return DOMDocument|null
   */
  public function CancelFeedSubmissions($feedSubmissionIdList = NULL,
  $feedTypeList = NULL,
  $submittedFromDate = NULL,
                                 $submittedToDate = NULL) {
    $this->useLibrary("Client");
    $this->useLibrary("CancelFeedSubmissionsRequest");

    $client = $this->spawnService("MarketplaceWebService_Client", array($this->access_key_id(),
      $this->secret_access_key(), $this->config(), $this->application_name(), $this->version_number(),
    ));
    $request = $this->spawnService("MarketplaceWebService_Model_CancelFeedSubmissionsRequest");

    if ($client && $request) {
      try {

        $request->setMerchant($this->seller_id());
        $request->setMWSAuthToken($this->auth_token());

        if ($feedSubmissionIdList) {
          $request->setFeedSubmissionIdList($feedSubmissionIdList);
        }
        if ($feedTypeList) {
          $request->setFeedTypeList($feedTypeList);
        }
        if ($submittedFromDate) {
          $request->setSubmittedFromDate($submittedFromDate);
        }
        if ($submittedToDate) {
          $request->setSubmittedToDate($submittedToDate);
        }

        $response = $client->CancelFeedSubmissions($request);
        return $this->processResponse($response);
      }
      catch (Exception $e) {
        $this->logging->error("%function returned '%exception' as error", array(
          "%function" => __FUNCTION__,
          "%exception" => $e->getMessage(),
        ));
      }
    }

    return NULL;
  }

  /**
   * http://docs.developer.amazonservices.com/en_CA/feeds/Feeds_GetFeedSubmissionCount.html.
   *
   * @param string $feedTypeList
   * @param string $feedProcessingStatusList
   * @param dateTime $submittedFromDate
   * @param dateTime $submittedToDate
   *
   * @return DOMDocument|null
   */
  public function GetFeedSubmissionCount($feedTypeList = NULL,
  $feedProcessingStatusList = NULL,
  $submittedFromDate = NULL,
                                  $submittedToDate = NULL) {
    $this->useLibrary("Client");
    $this->useLibrary("GetFeedSubmissionCountRequest");

    $client = $this->spawnService("MarketplaceWebService_Client", array($this->access_key_id(),
      $this->secret_access_key(), $this->config(), $this->application_name(), $this->version_number(),
    ));
    $request = $this->spawnService("MarketplaceWebService_Model_GetFeedSubmissionCountRequest");

    if ($client && $request) {
      try {

        $request->setMerchant($this->seller_id());
        $request->setMWSAuthToken($this->auth_token());

        if ($feedTypeList) {
          $request->setFeedTypeList($feedTypeList);
        }
        if ($feedProcessingStatusList) {
          $request->setFeedProcessingStatusList($feedProcessingStatusList);
        }
        if ($submittedFromDate) {
          $request->setSubmittedFromDate($submittedFromDate);
        }
        if ($submittedToDate) {
          $request->setSubmittedToDate($submittedToDate);
        }

        $response = $client->GetFeedSubmissionCount($request);
        return $this->processResponse($response);
      }
      catch (Exception $e) {
        $this->logging->error("%function returned '%exception' as error", array(
          "%function" => __FUNCTION__,
          "%exception" => $e->getMessage(),
        ));
      }
    }

    return NULL;
  }

  /**
   * http://docs.developer.amazonservices.com/en_CA/feeds/Feeds_GetFeedSubmissionList.html.
   *
   * @param string $feedSubmissionIdList
   * @param int $maxCount
   * @param string $feedTypeList
   * @param string $feedProcessingStatusList
   * @param dateTime $submittedFromDate
   * @param dateTime $submittedToDate
   *
   * @return DOMDocument|null
   */
  public function GetFeedSubmissionList($feedSubmissionIdList = NULL,
  $maxCount = NULL,
  $feedTypeList = NULL,
  $feedProcessingStatusList = NULL,
  $submittedFromDate = NULL,
  $submittedToDate = NULL) {
    $this->useLibrary("Client");
    $this->useLibrary("GetFeedSubmissionListRequest");

    $client = $this->spawnService("MarketplaceWebService_Client", array($this->access_key_id(),
      $this->secret_access_key(), $this->config(), $this->application_name(), $this->version_number(),
    ));
    $request = $this->spawnService("MarketplaceWebService_Model_GetFeedSubmissionListRequest");

    if ($client && $request) {
      try {

        $request->setMerchant($this->seller_id());
        $request->setMWSAuthToken($this->auth_token());

        if ($feedSubmissionIdList) {
          $request->setFeedSubmissionIdList($feedSubmissionIdList);
        }
        if ($maxCount) {
          $request->setMaxCount($maxCount);
        }
        if ($feedTypeList) {
          $request->setFeedTypeList($feedTypeList);
        }
        if ($feedProcessingStatusList) {
          $request->setFeedProcessingStatusList($feedProcessingStatusList);
        }
        if ($submittedFromDate) {
          $request->setSubmittedFromDate($submittedFromDate);
        }
        if ($submittedToDate) {
          $request->setSubmittedToDate($submittedToDate);
        }

        $response = $client->GetFeedSubmissionList($request);
        return $this->processResponse($response);
      }
      catch (Exception $e) {
        $this->logging->error("%function returned '%exception' as error", array(
          "%function" => __FUNCTION__,
          "%exception" => $e->getMessage(),
        ));
      }
    }

    return NULL;
  }

  /**
   * http://docs.developer.amazonservices.com/en_CA/feeds/Feeds_GetFeedSubmissionListByNextToken.html.
   *
   * @param string $nextToken
   *
   * @return DOMDocument|null
   */
  public function GetFeedSubmissionListByNextToken($nextToken) {

    $this->useLibrary("Client");
    $this->useLibrary("GetFeedSubmissionListByNextTokenRequest");

    $client = $this->spawnService("MarketplaceWebService_Client", array($this->access_key_id(),
      $this->secret_access_key(), $this->config(), $this->application_name(), $this->version_number(),
    ));
    $request = $this->spawnService("MarketplaceWebService_Model_GetFeedSubmissionListByNextTokenRequest");

    if ($client && $request) {
      try {

        $request->setMerchant($this->seller_id());
        $request->setMWSAuthToken($this->auth_token());
        $request->setNextToken($nextToken);

        $response = $client->GetFeedSubmissionListByNextToken($request);
        return $this->processResponse($response);
      }
      catch (Exception $e) {
        $this->logging->error("%function returned '%exception' as error", array(
          "%function" => __FUNCTION__,
          "%exception" => $e->getMessage(),
        ));
      }
    }

    return NULL;
  }

  /**
   * http://docs.developer.amazonservices.com/en_CA/feeds/Feeds_GetFeedSubmissionResult.html.
   *
   * @param string $feedSubmissionId
   *
   * @return DOMDocument|null
   */
  public function GetFeedSubmissionResult($feedSubmissionId) {
    $this->useLibrary("Client");
    $this->useLibrary("GetFeedSubmissionResult");

    $client = $this->spawnService("MarketplaceWebService_Client", array($this->access_key_id(),
      $this->secret_access_key(), $this->config(), $this->application_name(), $this->version_number(),
    ));
    $request = $this->spawnService("MarketplaceWebService_Model_GetFeedSubmissionResultRequest");

    if ($client && $request) {
      try {

        $request->setMerchant($this->seller_id());
        $request->setMWSAuthToken($this->auth_token());

        $request->setFeedSubmissionId($feedSubmissionId);

        $response = $client->GetFeedSubmissionResult($request);
        return $this->processResponse($response);
      }
      catch (Exception $e) {
        $this->logging->error("%function returned '%exception' as error", array(
          "%function" => __FUNCTION__,
          "%exception" => $e->getMessage(),
        ));
      }
    }

    return NULL;
  }

  /**
   * http://docs.developer.amazonservices.com/en_CA/feeds/Feeds_SubmitFeed.html.
   *
   * @param http -body $feedContent
   * @param string $feedType
   * @param string $marketplaceIdList
   * @param string $purgeAndReplace
   *
   * @return DOMDocument|null
   */
  public function SubmitFeed($feedContent, $feedType, $marketplaceIdList = NULL, $purgeAndReplace = NULL) {

    $this->useLibrary("Client");
    $this->useLibrary("SubmitFeedRequest");
    $this->useLibrary("IdList");

    $client = $this->spawnService("MarketplaceWebService_Client", array($this->access_key_id(),
      $this->secret_access_key(), $this->config(), $this->application_name(), $this->version_number(),
    ));
    $request = $this->spawnService("MarketplaceWebService_Model_SubmitFeedRequest");

    if ($client && $request) {
      try {

        $request->setMerchant($this->seller_id());
        $request->setMWSAuthToken($this->auth_token());

        $request->setFeedContent($feedContent);
        $request->setFeedType($feedType);

        // Request requires a content md5 header to work, constructed from the file content.
        $request->ContentMd5 = base64_encode(md5(stream_get_contents($feedContent), TRUE));

        if ($marketplaceIdList) {
          $marketplaceIdList = array('Id' => $marketplaceIdList);
          $request->setMarketplaceIdList($marketplaceIdList);
        }

        if ($purgeAndReplace) {
          $request->setPurgeAndReplace($purgeAndReplace);
        }

        $response = $client->SubmitFeed($request);
        return $this->processResponse($response);
      }
      catch (Exception $e) {
        $this->logging->error("%function returned '%exception' as error", array(
          "%function" => __FUNCTION__,
          "%exception" => $e->getMessage(),
        ));
      }
    }

    return NULL;
  }

  /**
   * Formats a feed for XML output.
   *
   * @param $data
   * @param $feed_type
   * @param $operation_type
   *   Optional operation type parameter, defaults to update, delete is used
   *   when deleting products.
   *
   * @return null|string
   */
  public function FormatFeed($data, $feed_type, $operation_type = 'Update') {
    module_load_include('inc', 'commerce_amazon_mws', 'includes/commerce_amazon_mws.feed_types');

    $feed = NULL;
    $xml_namespace = "xmlns:xsi";
    $xml_namespace_uri = "http://www.w3.org/2001/XMLSchema-instance";
    $schema_location = "xsi:noNamespaceSchemaLocation";
    $schema_location_xsd = "amzn-envelope.xsd";

    $feed = array(
      'Header' => array(
        'DocumentVersion' => '1.01',
        'MerchantIdentifier' => $this->merchant_token(),
      ),
      'MessageType' => 'Product',
    );

    switch ($feed_type) {
      // New Item.
      case PRODUCT_FEED:
        $feed['MessageType'] = 'Product';
        break;

      // Product Images.
      case PRODUCT_IMAGES_FEED:
        $feed['MessageType'] = 'ProductImage';
        break;

      // Stock Quantity Changes.
      case INVENTORY_FEED:
        $feed['MessageType'] = 'Inventory';
        break;

      // Relationship (Variation) mapping.
      case RELATIONSHIPS_FEED:
        $feed['MessageType'] = 'Relationship';
        break;

      // Item Price Changes.
      case PRICING_FEED:
        $feed['MessageType'] = 'Price';
        break;

      default:
        break;
    }

    foreach ($data as $idx => $row) {
      $feed['Message'][$idx]['MessageID'] = $idx + 1;
      $feed['Message'][$idx]['OperationType'] = $operation_type;
      $feed['Message'][$idx][$feed['MessageType']] = $row;
    }

    if ($feed) {
      $xml = CommerceAmazonMWSArray2XML::createXML('AmazonEnvelope', $feed);
      $envelope = $xml->documentElement;
      $envelope->setAttribute($xml_namespace, $xml_namespace_uri);
      $envelope->setAttribute($schema_location, $schema_location_xsd);

      return ($xml->saveXML());
    }

    return NULL;
  }

  /**
   * Returns the config required to setup the product client.
   *
   * @return array
   */
  protected function config() {
    return $this->defaultConfig("https://mws.amazonservices.com/");
  }

}
