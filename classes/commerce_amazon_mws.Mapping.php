<?php

/**
 * @file commerce_amazon_mws.Mapping.php
 */

/**
 * Creates the mapping from the JSON config file.
 */
class CommerceAmazonMWSTypeMapping {

  public $product_type;
  public $display_node;
  public $variation;

  protected $config_path;
  protected $product_fields;
  protected $transposed;
  protected $logging;
  protected $node;
  protected $category_map;
  protected $feed_type;
  protected $json_file;
  protected $list_on_amazon;
  protected $quantity_field;
  protected $conditionals;
  protected $alternate_parent_sku_field;
  protected $alternate_sku_field;

  protected $current_variation;

  /**
   * CommerceAmazonMWSTypeMapping constructor.
   *
   * @param $nid
   * @param string $feedType
   */
  public function __construct($nid, $feedType = "Product") {
    if ($this->node = entity_metadata_wrapper('node', $nid)) {
      $this->display_node = $this->node->getBundle();
    }

    $this->variation = FALSE;
    $this->category_map = FALSE;
    $this->json_file = NULL;
    $this->transposed = NULL;
    $this->product_type = "";
    $this->feed_type = $feedType;
    $this->current_variation = NULL;
    $this->list_on_amazon = NULL;
    $this->quantity_field = NULL;
    $this->conditionals = NULL;
    $this->alternate_parent_sku_field = NULL;
    $this->alternate_sku_field = NULL;

    $this->conf_path = drupal_get_path('module', 'commerce_amazon_mws') . '/includes/commerce_amazon_mws.map.json';
    $this->logging = new CommerceAmazonMWSLogging();

    $this->loadConfig();
  }

  /**
   * Prepares individual product variations from the loaded node.
   *
   * @param bool $ignore_list_flag
   *   Ignore a present list flag; used when deleting items.
   */
  protected function transpose($ignore_list_flag = FALSE) {
    $data = array();
    if (!$this->node) {
      $this->logging->warning("No node found for nid %item when transposing, skipping",
        array("%item" => $this->getNode()->nid));
      return;
    }

    // Skip on no variation.
    if (empty($this->variation) && $this->feed_type !== "ProductParent") {
      $this->logging->warning("No product variation field set for item %item, skipping",
        array("%item" => $this->getNode()->nid));
    }
    elseif ($variations = $this->getField($this->variation, $this->node)) {
      if ($this->feed_type !== "ProductParentDelete") {
        // Transpose each of the variation fields into the data array.
        foreach ($variations as $idx => $variation) {
          // Store the current variation so we can refer back to it.
          $this->current_variation = $variation;
          // Make sure either the node or variation is set to list on amazon, or continue
          // if there is no actual flag field.
          if ($ignore_list_flag || !$this->list_on_amazon || $this->getFieldValue($this->list_on_amazon, '')) {
            foreach ($this->getFields() as $amazon_field_key => $drupal_field_key) {
              // Avoid empty tags or NULL values.
              $value = $this->transposeVariation($variation, $amazon_field_key, $drupal_field_key);
              if (!is_null($value)) {
                $data[$idx][$amazon_field_key] = $value;
              }
            }
          }
        }

        // Parse through any conditionals set for this display type.
        $this->checkConditionals($data, $this->node->getBundle());

        // Now reset the current variation.
        $this->current_variation = NULL;
      }
    }

    // ProductParent feed does not use a variation like other feeds, so we build it out using node fields
    // the ProductParent feed shouldn't be called directly, only through the Product feed.
    if ($this->feed_type === "ProductParent" || $this->feed_type === "ProductParentDelete") {
      // Make sure either the node or variation is set to list on amazon, or continue if there is no actual flag field.
      if ($ignore_list_flag || !$this->list_on_amazon || $this->getFieldValue($this->list_on_amazon, '')) {
        foreach ($this->getFields() as $amazon_field_key => $drupal_field_key) {
          // !note that we use the node here, not the variation, however we still use the transpose variation function
          // avoid empty tags or NULL values.
          $value = $this->transposeVariation($this->node, $amazon_field_key, $drupal_field_key);
          if (!is_null($value)) {
            $data[$amazon_field_key] = $value;
          }
        }
      }
    }

    // If the feed is Product, then we need to get a ProductParent, using the ProductParent Feed.
    if ($this->feed_type == "Product") {
      $product_parent_mapping = new CommerceAmazonMWSTypeMapping($this->node->nid->value(), "ProductParent");
      $parent_data = $product_parent_mapping->getTransposed($ignore_list_flag);
      // Once we have the product parent, we need to give it product data to define the category and
      // variation theme, we also need to strip any variation attributes other than VariationTheme from
      // the VariationData, and add a Parentage element to VariationData defined as variation-parent.
      if (isset($data[0]['ProductData'])) {
        $parent_data['ProductData'] = $data[0]['ProductData'];
        $this->_setProductParent($parent_data);
        array_unshift($data, $parent_data);
      }
    }

    if ($this->feed_type === "ProductDelete") {
      $product_parent_mapping = new CommerceAmazonMWSTypeMapping($this->node->nid->value(), "ProductParentDelete");
      $parent_data = $product_parent_mapping->getTransposed($ignore_list_flag);
      if (isset($parent_data[0])) {
        $data[] = $parent_data[0];
      }
    }

    $this->transposed = $data;
  }

  /**
   * Strips out any VariationData attributes, setting Parentage to
   * variation-parent and leaving the VariationTheme in place.
   *
   * @param $parent_data
   */
  protected function _setProductParent(&$parent_data, $parent_key = NULL) {
    foreach ($parent_data as $field_title => &$field) {
      if (is_array($field)) {
        $this->_setProductParent($field, $field_title);
      }

      if (array_key_exists("VariationData", $parent_data)) {
        if (!isset($parent_data["VariationData"]["Parentage"])
          || $parent_data["VariationData"]["Parentage"] != "parent") {
          $parent_data["VariationData"]["Parentage"] = "parent";
          ksort($parent_data["VariationData"]);
        }
      }

      if ($parent_key === "VariationData") {
        if ($field_title !== "VariationTheme" && $field_title !== "Parentage") {
          unset($parent_data[$field_title]);
        }
      }
    }
  }

  /**
   * Filters/modifies transposed data in place through conditions.
   *
   * @param array $data
   * @param string $display_node_type
   *   product_display.
   *
   * @TODO this is still in early stages, check the README
   */
  public function checkConditionals(&$data, $display_node_type) {
    if ($this->conditionals && isset($this->conditionals[$display_node_type])) {
      // Get conditionals for this display type.
      $conditionals = $this->conditionals[$display_node_type];
      // Recurse through data fields until we match one of the conditional fields.
      foreach ($conditionals as $condition_field_name => $condition) {
        if ($this->_matchConditional($data, $condition_field_name)) {
          $this->_applyConditional($data, $condition, $condition_field_name);
        }
      }
    }
  }

  /**
   * Handles passed conditions.
   *
   * @param $data
   * @param $condition
   */
  protected function _applyConditional(&$data, $condition) {
    // Handle if conditions.
    if (isset($condition['if'])) {
      foreach ($condition['if'] as $if) {
        // Handle not condition.
        $trigger = FALSE;
        if (isset($if['not'])) {
          $trigger = !$this->_matchConditional($data, $if['not']);
        }
        elseif (isset($if['is'])) {
          $trigger = $this->_matchConditional($data, $if['not']);
        }
        // Handle then actions in sequence.
        if ($trigger && $if['then']) {
          // Handle all of the then actions.
          foreach ($if['then'] as $action => $value) {
            $this->_thenConditional($data, $action, $value);
          }
        }
      }
    }
  }

  /**
   * Applies the results of a 'then' step in the conditional.
   *
   * @param $data
   * @param $action
   * @param $value
   *
   * @TODO can only handle the set action on conditional at the moment, check the README
   */
  protected function _thenConditional(&$data, $action, $value) {

    // If the action is 'set', loop each 'set' set of key,values.
    if ($action == 'set') {
      foreach ($value as $set_values) {

        foreach ($data as $field_name => &$field) {

          if (is_array($field)) {
            $this->_thenConditional($field, $action, $value);
          }

          // If the field name matches one of the set field names, change the value of it.
          if ($field_name === $set_values[0]) {
            $field = $set_values[1];
          }
        }
      }
    }
  }

  /**
   * Checks that the field exists in the prescribed data.
   *
   * @param $data
   * @param $condition_field
   *
   * @return bool
   */
  protected function _matchConditional($data, $condition_field) {
    $match = FALSE;

    foreach ($data as $field_name => $field) {
      if (is_array($field)) {
        $match = $this->_matchConditional($field, $condition_field);
      }

      if ($field_name === $condition_field) {
        $match = TRUE;
      }
    }

    return $match;
  }

  /**
   * Determines which part of the mapping will be transposed and stored.
   *
   * @param $feedType
   *   Product
   *   ProductImage
   *   Inventory
   *   Relationship
   */
  public function setFeedType($feedType = "NewProduct") {
    $this->transposed = NULL;
    $this->feed_type = $feedType;

    // Reload the config for the feed type.
    $this->loadConfig();
  }

  /**
   * Transposes or gets the last transposed contents
   * of the mapped node and its variations.
   *
   * @param bool $ignore_list_flag
   *   choose to ignore the list flag; used when deleting products.
   *
   * @return null|string
   */
  public function getTransposed($ignore_list_flag = FALSE) {
    if (empty($this->transposed)) {
      $this->transpose($ignore_list_flag);
    }

    // Override if the feed type is a Relationship feed, as all variation
    // relationships need to be placed together with one SKU.
    if ($this->feed_type == "Relationship") {

      $relations = array();
      $parent_sku = NULL;

      foreach ($this->transposed as $entry) {
        if (!$parent_sku) {
          $parent_sku = $entry['ParentSKU'];
        }
        $relations[] = $entry['Relation'];
      }

      $this->transposed = array(
        array(
          'ParentSKU' => $parent_sku,
          'Relation' => $relations,
        ),
      );
    }

    return $this->transposed;
  }

  /**
   * Returns transposed data to XML or HTML.
   *
   * @param $html
   *
   * @return null|string
   *   XML unless $html arg == TRUE
   */
  public function getMarkup($html = FALSE) {
    if ($transposed = $this->getTransposed()) {
      try {
        $xml = array();
        $xml_root = $this->feed_type;

        // Set the right xml root node name for the right feed type.
        if ($xml_root) {
          foreach ($transposed as $data) {
            $data_xml = CommerceAmazonMWSArray2XML::createXML($xml_root, $data);
            $xml[] = ($html) ? $data_xml->saveHTML() : $data_xml->saveXML();
          }
        }

        return $xml;
      }
      catch (Exception $e) {
        $this->logging->error("%function returned '%exception' as error", array(
          "%function" => __FUNCTION__,
          "%exception" => $e->getMessage(),
        ));
      }
    }

    return NULL;
  }

  /**
   * Returns the correct field value from the node or variation.
   *
   * @param $field_id
   * @param $field_key
   *
   * @return bool|null|string
   */
  protected function getFieldValue($field_id, $field_key) {

    if (is_string($field_id)) {

      // Match node fields.
      preg_match('/\%(.+|.+\-\>)\%/', $field_id, $matches);
      if (isset($matches[1])) {
        // If the field is a sku/parent sku field, return an alternate sku if one is
        // set or available.
        if ($alternate_sku = $this->checkAlternateSKU($field_key, $this->node, 'parent')) {
          return $alternate_sku;
        }

        // Check if the base field exists only, don't include nested values identified with ->.
        $base_field = preg_replace('/\-\>.+/', '', $matches[1]);
        if ($this->entityHasField('node', $this->display_node, $base_field)) {
          return $this->getField($matches[1], $this->node, $field_key);
        }

        return NULL;
      }

      // Match variation fields.
      preg_match('/\$(.+|.+\-\>)\$/', $field_id, $matches);
      if (isset($matches[1])) {
        $wrapper = entity_metadata_wrapper('commerce_product', $this->current_variation);

        // If the field is a sku/parent sku field, return an alternate sku if one is
        // set or available.
        if ($alternate_sku = $this->checkAlternateSKU($field_key, $wrapper, 'child')) {
          return $alternate_sku;
        }

        // Check if the base field exists only, don't include nested values identified with ->.
        $base_field = preg_replace('/\-\>.+/', '', $matches[1]);
        if ($this->entityHasField('commerce_product', $wrapper->getBundle(), $base_field)) {
          return $this->getField($matches[1], $wrapper, $field_key);
        }

        return NULL;
      }

      // Match computed fields.
      preg_match('/\&(.+)\&/', $field_id, $matches);
      if (isset($matches[1])) {
        return $this->getComputedField($matches[1]);
      }

    }

    return $field_id;
  }

  /**
   * Checks whether entity type has a specified field or not.
   *
   * @param $entity_type
   * @param $bundle
   * @param $field_name
   *
   * @return bool
   */
  protected function entityHasField($entity_type, $bundle, $field_name) {
    $instances = field_info_instances($entity_type);

    if (isset($instances[$bundle])) {
      $instances[$bundle] += field_info_extra_fields($entity_type, $bundle, 'form');
      if (array_key_exists($field_name, $instances[$bundle])) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Gets the value of a wrapper field by field name,
   * checking the field type and filtering for an
   * appropriate value. If an amazon field name is
   * provided, a different value may be returned
   * even if a field is used multiple times per instance.
   *  i.e a weight, height and length amazon field may
   *  use the same product dimensions field, checking
   *  by amazon field name will return the correct value.
   *
   * @param $field_name
   * @param $context_wrapper
   * @param null $amazon_field_name
   *
   * @return bool|null|string
   */
  protected function getField($field_name, $context_wrapper, $amazon_field_name = NULL) {

    // Check here to see if we have a field with a nested value set up i.e. field_product_dimensions->length
    // we'll use this as a specifier to get a value from the field value result. i.e. $result_array['length'];.
    $nested = NULL;
    preg_match('/(.+)\-\>(.+)/', $field_name, $nested_matches);
    if (count($nested_matches) > 2) {
      $field_name = $nested_matches[1];
      $nested = $nested_matches[2];
    };

    // Make sure the field exists for the node first.
    if (isset($context_wrapper->{$field_name})) {
      // Get the right value for field type.
      $field_info = field_info_field($field_name);
      switch ($field_info['type']) {

        case 'physical_dimensions':
          $dimensions = $context_wrapper->{$field_name}->value();
          if ($nested && isset($dimensions[$nested])) {
            return strtoupper($dimensions[$nested]);
          }
          return $dimensions;

        case 'number_decimal':
          // don't allow less than 0 on quantities.
          if ($amazon_field_name == 'NumberOfItems' || $amazon_field_name == 'Quantity') {
            $val = (int) $context_wrapper->{$field_name}->value();
            if ($val < 0) {
              $val = 0;
            }

            return (string) $val;
          }

          return $context_wrapper->{$field_name}->value();

        case 'text':
          return $context_wrapper->{$field_name}->value();

        case 'text_with_summary':
          // We want to get the regular value for the text instead
          // of the safe value so we need to make sure it's not empty first.
          $value = $context_wrapper->{$field_name}->value();
          if (isset($value['value'])) {
            // The description field cannot have HTML, so we need to strip it of tags.
            if ($amazon_field_name == 'Description') {
              $val = $context_wrapper->{$field_name}->value->value();
              return strip_tags($val);
            }

            return $context_wrapper->{$field_name}->value->value();
          }

          return NULL;

        case 'taxonomy_term_reference':
          $term = $this->node->{$field_name}->value();
          $term = (is_array($term)) ? $term[0] : $term;

          // Map to Amazon category on ProductData.
          if ($amazon_field_name == 'ProductData') {
            // Construct ProductData; if we don't get any we must return NULL.
            $term_category = $this->termToProductData($term);
            if (count($term_category)) {
              return array($term_category);
            }

            return NULL;
          }

          return ($term) ? $term->name : NULL;

        case 'physical_weight':
          $weight = $context_wrapper->{$field_name}->value();
          if ($nested && !empty($weight[$nested])) {
            return strtoupper($weight[$nested]);
          }

          return strtoupper($weight['weight']);

        case 'commerce_price':
          $currency = $context_wrapper->{$field_name}->value();

          if ($amazon_field_name == 'currency') {
            return $currency['currency_code'];
          }
          if ($amazon_field_name == 'value' || $amazon_field_name == '@value') {
            return commerce_currency_amount_to_decimal($currency['amount'], $currency['currency_code']);
          }

          return $currency;

        case 'image':
          // Always default image to the public URL, do an if statement here on the
          // amazon type to do something more unique.
          $image = $context_wrapper->{$field_name}->value();
          if (isset($image[0])) {
            return file_create_url($image[0]['uri']);
          }

          return NULL;

        default:
          return $context_wrapper->{$field_name}->value();
      }
    }

    return NULL;
  }

  /**
   * Computes a mapped field.
   *
   * @param $field_name
   *   Any leading or trailing '&' gets stripped before computation.
   *
   * @return null|string
   */
  protected function getComputedField($field_name) {

    // Trim any '&' prefix or suffix before continuing.
    $field_name = trim($field_name, '&');

    switch ($field_name) {

      case 'node_url':
        global $base_url;
        return $base_url . '/' . drupal_get_path_alias('node/' . $this->getNode()->nid);

      case 'unavailable_on_zero_qty':
        if ($this->current_variation && $this->quantity_field) {
          $field_val = $this->getFieldValue($this->quantity_field, 'Quantity');
          if ($field_val < 1) {
            return "false";
          }
        }
        return "true";

      default:
        return NULL;
    }
  }

  /**
   * Transposes each variation individually and recursively, unlike transpose().
   *
   * @param $variation
   * @param $amazon_field_key
   * @param $drupal_field_key
   *
   * @return array|null
   */
  protected function transposeVariation($variation, $amazon_field_key, $drupal_field_key) {

    if (is_string($drupal_field_key)) {
      $field_value = $this->getFieldValue($drupal_field_key, $amazon_field_key);

      // We have to do this weird checking for 0 or a value here, because we
      // want 0s to display, but we don't want empty strings.
      return ($field_value || $field_value == 0) ? $field_value : NULL;
    }
    // Recurse all nested values.
    if (is_array($drupal_field_key)) {
      $data = array();
      foreach ($drupal_field_key as $nested_amazon_field_key => $nested_drupal_field_key) {
        if ($field_value = $this->transposeVariation($variation, $nested_amazon_field_key, $nested_drupal_field_key)) {
          $data[$nested_amazon_field_key] = $field_value;
        }
      }
      return ($data) ? $data : NULL;
    }

    return NULL;
  }

  /**
   * Returns the value from the node metadata wrapper.
   *
   * @return mixed
   *
   * @throws EntityMetadataWrapperException
   */
  public function getNode() {
    return $this->node->value();
  }

  /**
   * Gets for the product fields.
   *
   * @return mixed
   */
  public function getFields() {
    return $this->product_fields;
  }

  /**
   * Checks if a node/variation has an alternate sku.
   * If it does, it sets the field and returns true
   * otherwise it skips over and returns false.
   * Works for ParentSKU and SKU only.
   *
   * @param $field_key
   *   the amazon field key i.e. SKU, ParentSKU
   *
   * @param $context
   *   a node or variation wrapper to pass to getFieldValue
   *
   * @param $relationship
   *   can be either 'child' or 'parent'
   *
   * @return bool
   */
  protected function checkAlternateSKU($field_key, $context, $relationship = 'child') {
    if ($field_key == 'SKU' || $field_key == 'ParentSKU') {
      if ($relationship == 'child' && $this->alternate_sku_field) {
        // Child should use $ prefix/suffix.
        preg_match('/\$(.+|.+\-\>)\$/', $this->alternate_sku_field, $matches);
        if (isset($matches[1])) {
          return $this->getField($matches[1], $context, $field_key);
        }
      }
      elseif ($relationship == 'parent' && $this->alternate_parent_sku_field) {
        // Parent should use % prefix/suffix.
        preg_match('/\%(.+|.+\-\>)\%/', $this->alternate_parent_sku_field, $matches);
        if (isset($matches[1])) {
          return $this->getField($matches[1], $context, $field_key);
        }
      }
    }

    return FALSE;
  }

  /**
   * Converts a taxonomy term to an Amazon category. This
   * creates the entire ProductData tree for a category, which uses the
   * Category-Specific XSDs.
   * https://sellercentral.amazon.com/gp/help/200385010?ie=UTF8&*Version*=1&*entries*=0&.
   *
   * @param $term
   *
   * @return array|null
   */
  protected function termToProductData($term) {

    // Overrides first (for children categories)
    if (!empty($this->category_map['override'])) {
      if (isset($this->category_map['override'][$term->name])) {
        return $this->getProductData($this->category_map['override'][$term->name]);
      }
    }

    // Main categories last.
    if (!empty($this->category_map['main'])) {
      if (isset($this->category_map['main'][$term->name])) {
        return $this->getProductData($this->category_map['main'][$term->name]);
      }
    }

    // If we don't have match, check the parent category instead until exhausted of parents.
    if ($parents = taxonomy_get_parents($term->tid)) {
      foreach ($parents as $parent) {
        if ($match = $this->termToProductData($parent)) {
          return $match;
        }
      }
    }

    return NULL;
  }

  /**
   * Returns a boolean value if the list on amazon field is checked.
   *
   * @return bool
   *
   * @TODO this function is a later edition, should be replaced in other areas like transpose function
   *       to use this function.
   */
  public function isListed() {
    return $this->getFieldValue($this->list_on_amazon, "");
  }

  /**
   * Exhausts each node of a mapping until it reaches a single value it can get
   * the field of. If it's an array, it will continue to drill down until it
   * reaches a final node.
   *
   * @param $data_context
   *
   * @return array
   */
  protected function getProductData($data_context) {

    foreach ($data_context as $key => $val) {
      if (is_array($val)) {
        $data_val = $this->getProductData($val);
        if (!empty($data_val)) {
          $data_context[$key] = $data_val;
        }
        // don't add null or empty values.
        else {
          unset($data_context[$key]);
        }
      }
      else {
        $field_val = $this->getFieldValue($val, $key);
        if (!empty($field_val)) {
          $data_context[$key] = $field_val;
        }
        // don't add null or empty values.
        else {
          unset($data_context[$key]);
        }
      }
    }

    return $data_context;
  }

  /**
   * Configures the instance from the config file.
   */
  protected function loadConfig() {
    // Load the config.
    if (!$this->json_file) {
      try {
        $this->json_file = file_get_contents($this->conf_path);
      }
      catch (Exception $e) {
        $this->logging->error("Unable to open the Amazon MWS config file: " . $this->conf_path
          . ". Check file contents or that file exists.");
      }
    }

    // Decode the file.
    if ($this->json_file && $json = json_decode($this->json_file, TRUE)) {

      // Check data for the feed type actually exists first.
      if (isset($json['display_node'][$this->display_node][$this->feed_type])) {
        // Set map variation name and product display fields.
        if (isset($json['display_node'][$this->display_node][$this->feed_type]['variation'])) {
          $this->variation = $json['display_node'][$this->display_node][$this->feed_type]['variation'];
        }

        if (isset($json['display_node'][$this->display_node][$this->feed_type]['fields'])) {
          $this->product_fields = $json['display_node'][$this->display_node][$this->feed_type]['fields'];
        }

        // Set the list_flag field name.
        if (isset($json['display_node'][$this->display_node][$this->feed_type]['list_flag'])) {
          $this->list_on_amazon = $json['display_node'][$this->display_node][$this->feed_type]['list_flag'];
        }
        // Get the quantity field for use with the unavailable_on_zero_qty computed field.
        if (isset($json['display_node'][$this->display_node][$this->feed_type]['fields']['Quantity'])) {
          $this->quantity_field = $json['display_node'][$this->display_node][$this->feed_type]['fields']['Quantity'];
        }
        // Get the alternate-sku fields if set.
        if (isset($json['display_node'][$this->display_node][$this->feed_type]['alternate_sku'])) {
          $this->alternate_sku_field = $json['display_node'][$this->display_node][$this->feed_type]['alternate_sku'];
        }
        if (isset($json['display_node'][$this->display_node][$this->feed_type]['alternate_parent_sku'])) {
          $this->alternate_parent_sku_field
            = $json['display_node'][$this->display_node][$this->feed_type]['alternate_parent_sku'];
        }

        if (isset($json['categories'])) {
          $this->category_map = $json['categories'];
        }

        if (isset($json['conditionals'])) {
          $this->conditionals = $json['conditionals'];
        }
      }
    }

    else {
      $this->logging->error("Amazon MWS config file: " . $this->conf_path . ". Does not contain valid JSON, please "
        . "check through a utility like JSON lint before using the Amazon MWS module.");
    }
  }

}
