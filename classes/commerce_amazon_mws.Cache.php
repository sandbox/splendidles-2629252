<?php

/**
 * @file commerce_amazon_mws.Cache.php
 */

/**
 * Class CommerceAmazonMWSCache.
 */
class CommerceAmazonMWSCache {

  /**
   * Returns a length of time an entry will sit before being removed.
   *
   * @return int
   */
  public static function cacheLifeTime() {
    return strtotime('-7 days');
  }

  /**
   * Gets data from the module cache by cache id and type.
   *
   * @param string $cache_id
   * @param string $cache_type
   *
   * @return array|null
   */
  public static function getCache($cache_id, $cache_type) {
    $data = db_select('amazon_mws_cache', 'c')
      ->fields('c')
      ->condition('cache_id', $cache_id, '=')
      ->condition('cache_type', $cache_type, '=')
      ->execute()
      ->fetchAssoc();

    if (isset($data['data'])) {
      $data = base64_decode($data['data']);
      return unserialize($data);
    }

    return NULL;
  }

  /**
   * Sets data from the module cache by cache id and type.
   *
   * @param string $cache_id
   * @param string $cache_type
   * @param $data
   *
   * @throws Exception
   * @throws InvalidMergeQueryException
   */
  public static function setCache($cache_id, $cache_type, $data) {
    if (!is_array($data) || unserialize($data) !== FALSE) {
      throw new Exception("CommerceAmazonMWSCache::setCache data must be an unserialized array");
    }
    // base64 encode here to catch any dodgy database encoding errors.
    $data = base64_encode(serialize($data));

    db_merge('amazon_mws_cache')
      ->key(array('cache_id' => $cache_id, 'cache_type' => $cache_type))
      ->fields(array(
        'cache_id' => $cache_id,
        'cache_type' => $cache_type,
        'data' => $data,
        'timestamp' => time(),
      ))
      ->execute();
  }

  /**
   * Deletes data from the module cache by cache id and type.
   *
   * @param string $cache_id
   * @param string $cache_type
   */
  public static function deleteCache($cache_id, $cache_type) {
    db_delete('amazon_mws_cache')
      ->condition('cache_id', $cache_id)
      ->condition('cache_type', $cache_type)
      ->execute();
  }

  /**
   * Flushes the module data cache.
   */
  public static function flushCache() {
    db_delete('amazon_mws_cache')
      ->execute();
  }

  /**
   * Removes cache entries older than a given expiry limit.
   *
   * @param int $lifetime
   * @param string $cache_type
   */
  public static function cleanCache($lifetime = NULL, $cache_type = NULL) {
    $query = db_delete('amazon_mws_cache')
      ->condition('timestamp', ($lifetime) ? $lifetime : CommerceAmazonMWSCache::cacheLifeTime(), '<');

    if ($cache_type) {
      $query->condition('cache_type', $cache_type, '=');
    }

    $query->execute();
  }

}
