<?php

/**
 * @file commerce_amazon_mws.Products.php
 */

/**
 * Class CommerceAmazonMwsProducts.
 */
class CommerceAmazonMwsProducts extends CommerceAmazonMWSService {

  /**
   * @inheritdoc
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   * @inheritdoc
   */
  protected function init() {
    $this->library_path = $_SERVER['DOCUMENT_ROOT'] . '/' . drupal_get_path('module', 'commerce_amazon_mws')
      . '/libraries/nonapod/AmazonMWS/MarketplaceWebServiceProducts';
    $this->loadLibraries();
  }

  /**
   * http://docs.developer.amazonservices.com/en_CA/products/Products_GetCompetitivePricingForASIN.html.
   *
   * @param array $ASINList
   *
   * @return DOMDocument|null
   */
  public function GetCompetitivePricingForASIN(array $ASINList) {
    $this->useLibrary("Client");
    $this->useLibrary("ASINListType");
    $this->useLibrary("GetCompetitivePricingForASINRequest");

    $client = $this->spawnService("MarketplaceWebServiceProducts_Client", array($this->access_key_id(),
      $this->secret_access_key(), $this->application_name(), $this->version_number(), $this->config(),
    ));
    $request = $this->spawnService("MarketplaceWebServiceProducts_Model_GetCompetitivePricingForASINRequest");

    if ($client && $request) {
      try {
        $asin_list = $this->spawnService("MarketplaceWebServiceProducts_Model_ASINListType");
        $asin_list->setASIN($ASINList);

        $request->setASINList($asin_list);
        $request->setSellerId($this->seller_id());
        $request->setMarketplaceId($this->marketplace_id());

        $response = $client->GetCompetitivePricingForASIN($request);

        return $this->processResponse($response);
      }
      catch (Exception $e) {
        $this->logging->error("%function returned '%exception' as error", array(
          "%function" => __FUNCTION__,
          "%exception" => $e->getMessage(),
        ));
      }
    }

    return NULL;
  }

  /**
   * http://docs.developer.amazonservices.com/en_CA/products/Products_GetCompetitivePricingForSKU.html.
   *
   * @param array $sellerSKUList
   *
   * @return DOMDocument|null
   */
  public function GetCompetitivePricingForSKU(array $sellerSKUList) {
    $this->useLibrary("Client");
    $this->useLibrary("SKUListType");
    $this->useLibrary("GetCompetitivePricingForSKURequest");

    $client = $this->spawnService("MarketplaceWebServiceProducts_Client", array($this->access_key_id(),
      $this->secret_access_key(), $this->application_name(), $this->version_number(), $this->config(),
    ));
    $request = $this->spawnService("MarketplaceWebServiceProducts_Model_GetCompetitivePricingForSKURequest");

    if ($client && $request) {
      try {
        $sku_list = $this->spawnService("MarketplaceWebServiceProducts_Model_SellerSKUListType");
        $sku_list->setSellerSKU($sellerSKUList);

        $request->setSellerSKUList($sku_list);
        $request->setSellerId($this->seller_id());
        $request->setMarketplaceId($this->marketplace_id());

        $response = $client->GetCompetitivePricingForSKU($request);
        return $this->processResponse($response);
      }
      catch (Exception $e) {
        $this->logging->error("%function returned '%exception' as error", array(
          "%function" => __FUNCTION__,
          "%exception" => $e->getMessage(),
        ));
      }
    }

    return NULL;
  }

  /**
   * http://docs.developer.amazonservices.com/en_CA/products/Products_LowestOfferListingsForASIN.html.
   *
   * @param array $ASINList
   * @param string $itemCondition
   *
   * @return DOMDocument|null
   */
  public function GetLowestOfferListingsForASIN(array $ASINList, $itemCondition = NULL) {
    $this->useLibrary("Client");
    $this->useLibrary("ASINListType");
    $this->useLibrary("GetLowestOfferListingsForASINRequest");

    $client = $this->spawnService("MarketplaceWebServiceProducts_Client", array($this->access_key_id(),
      $this->secret_access_key(), $this->application_name(), $this->version_number(), $this->config(),
    ));
    $request = $this->spawnService("MarketplaceWebServiceProducts_Model_GetLowestOfferListingsForASINRequest");

    if ($client && $request) {
      try {
        $asin_list = $this->spawnService("MarketplaceWebServiceProducts_Model_ASINListType");
        $asin_list->setASIN($ASINList);

        if ($itemCondition) {
          $request->setItemCondition($itemCondition);
        }

        $request->setASINList($asin_list);
        $request->setSellerId($this->seller_id());
        $request->setMarketplaceId($this->marketplace_id());

        $response = $client->GetLowestOfferListingsForASIN($request);
        return $this->processResponse($response);
      }
      catch (Exception $e) {
        $this->logging->error("%function returned '%exception' as error", array(
          "%function" => __FUNCTION__,
          "%exception" => $e->getMessage(),
        ));
      }
    }

    return NULL;
  }

  /**
   * http://docs.developer.amazonservices.com/en_CA/products/Products_LowestOfferListingsForSKU.html.
   *
   * @param array $sellerSKUList
   * @param string $itemCondition
   *
   * @return DOMDocument|null
   */
  public function GetLowestOfferListingsForSKU(array $sellerSKUList, $itemCondition = NULL) {
    $this->useLibrary("Client");
    $this->useLibrary("SellerSKUListType");
    $this->useLibrary("GetLowestOfferListingsForSKURequest");

    $client = $this->spawnService("MarketplaceWebServiceProducts_Client", array($this->access_key_id(),
      $this->secret_access_key(), $this->application_name(), $this->version_number(), $this->config(),
    ));
    $request = $this->spawnService("MarketplaceWebServiceProducts_Model_GetLowestOfferListingsForSKURequest");

    if ($client && $request) {
      try {
        $sku_list = $this->spawnService("MarketplaceWebServiceProducts_Model_SellerSKUListType");
        $sku_list->setSellerSKU($sellerSKUList);

        if ($itemCondition) {
          $request->setItemCondition($itemCondition);
        }

        $request->setSellerSKUList($sku_list);
        $request->setSellerId($this->seller_id());
        $request->setMarketplaceId($this->marketplace_id());

        $response = $client->GetLowestOfferListingsForSKU($request);

        return $this->processResponse($response);
      }
      catch (Exception $e) {
        $this->logging->error("%function returned '%exception' as error", array(
          "%function" => __FUNCTION__,
          "%exception" => $e->getMessage(),
        ));
      }
    }

    return NULL;
  }

  /**
   * http://docs.developer.amazonservices.com/en_CA/products/Products_GetLowestPricedOffersForASIN.html.
   *
   * @param string $ASIN
   * @param string $itemCondition
   *
   * @return DOMDocument|null
   */
  public function GetLowestPricedOffersForASIN($ASIN, $itemCondition) {
    $this->useLibrary("Client");
    $this->useLibrary("GetLowestPricedOffersForASINRequest");

    $client = $this->spawnService("MarketplaceWebServiceProducts_Client", array($this->access_key_id(),
      $this->secret_access_key(), $this->application_name(), $this->version_number(), $this->config(),
    ));
    $request = $this->spawnService("MarketplaceWebServiceProducts_Model_GetLowestPricedOffersForASINRequest");

    if ($client && $request) {
      try {
        $request->setItemCondition($itemCondition);
        $request->setASIN($ASIN);
        $request->setSellerId($this->seller_id());
        $request->setMarketplaceId($this->marketplace_id());

        $response = $client->GetLowestPricedOffersForASIN($request);

        return $this->processResponse($response);
      }
      catch (Exception $e) {
        $this->logging->error("%function returned '%exception' as error", array(
          "%function" => __FUNCTION__,
          "%exception" => $e->getMessage(),
        ));
      }
    }

    return NULL;
  }

  /**
   * http://docs.developer.amazonservices.com/en_CA/products/Products_GetLowestPricedOffersForSKU.html.
   *
   * @param string $sellerSKU
   * @param string $itemCondition
   *
   * @return DOMDocument|null
   */
  public function GetLowestPricedOffersForSKU($sellerSKU, $itemCondition) {
    $this->useLibrary("Client");
    $this->useLibrary("GetLowestPricedOffersForSKURequest");

    $client = $this->spawnService("MarketplaceWebServiceProducts_Client", array($this->access_key_id(),
      $this->secret_access_key(), $this->application_name(), $this->version_number(), $this->config(),
    ));
    $request = $this->spawnService("MarketplaceWebServiceProducts_Model_GetLowestPricedOffersForSKURequest");

    if ($client && $request) {
      try {
        $request->setItemCondition($itemCondition);
        $request->setSellerSKU($sellerSKU);
        $request->setSellerId($this->seller_id());
        $request->setMarketplaceId($this->marketplace_id());

        $response = $client->GetLowestPricedOffersForSKU($request);

        return $this->processResponse($response);
      }
      catch (Exception $e) {
        $this->logging->error("%function returned '%exception' as error", array(
          "%function" => __FUNCTION__,
          "%exception" => $e->getMessage(),
        ));
      }
    }

    return NULL;
  }

  /**
   * http://docs.developer.amazonservices.com/en_CA/products/Products_GetMatchingProduct.html.
   *
   * @param array $ASINList
   *
   * @return DOMDocument|null
   */
  public function GetMatchingProduct(array $ASINList) {
    $this->useLibrary("Client");
    $this->useLibrary("ASINListType");
    $this->useLibrary("GetMatchingProductRequest");

    $client = $this->spawnService("MarketplaceWebServiceProducts_Client", array($this->access_key_id(),
      $this->secret_access_key(), $this->application_name(), $this->version_number(), $this->config(),
    ));
    $request = $this->spawnService("MarketplaceWebServiceProducts_Model_GetMatchingProductRequest");

    if ($client && $request) {
      try {
        $asin_list = $this->spawnService("MarketplaceWebServiceProducts_Model_ASINListType");
        $asin_list->setASIN($ASINList);

        $request->setASINList($asin_list);
        $request->setSellerId($this->seller_id());
        $request->setMarketplaceId($this->marketplace_id());

        $response = $client->GetMatchingProduct($request);

        return $this->processResponse($response);
      }
      catch (Exception $e) {
        $this->logging->error("%function returned '%exception' as error", array(
          "%function" => __FUNCTION__,
          "%exception" => $e->getMessage(),
        ));
      }
    }

    return NULL;
  }

  /**
   * http://docs.developer.amazonservices.com/en_CA/products/Products_GetMatchingProductForId.html.
   *
   * @param $idType
   * @param $idList
   *
   * @return DOMDocument|null
   */
  public function GetMatchingProductForId($idType, $idList) {

    if ($idType == "ASIN") {
      return $this->GetMatchingProduct($idList);
    }

    $this->useLibrary("Client");
    $this->useLibrary("GetMatchingProductForIdRequest");

    $client = $this->spawnService("MarketplaceWebServiceProducts_Client", array($this->access_key_id(),
      $this->secret_access_key(), $this->application_name(), $this->version_number(), $this->config(),
    ));
    $request = $this->spawnService("MarketplaceWebServiceProducts_Model_GetMatchingProductForIdRequest");

    if ($client && $request) {
      try {
        $request->setSellerId($this->seller_id());
        $request->setMarketplaceId($this->marketplace_id());
        $request->setIdType($idType);
        $request->setIdList($idType);

        $response = $client->GetServiceStatus($request);
        return $this->processResponse($response);
      }
      catch (Exception $e) {
        $this->logging->error("%function returned '%exception' as error", array(
          "%function" => __FUNCTION__,
          "%exception" => $e->getMessage(),
        ));
      }
    }

    return NULL;
  }

  /**
   * http://docs.developer.amazonservices.com/en_CA/products/Products_GetMyPriceForASIN.html.
   *
   * @param array $ASINList
   * @param string $itemCondition
   *
   * @return DOMDocument|null
   */
  public function GetMyPriceForASIN(array $ASINList, $itemCondition = NULL) {
    $this->useLibrary("Client");
    $this->useLibrary("ASINListType");
    $this->useLibrary("GetMyPriceForASINRequest");

    $client = $this->spawnService("MarketplaceWebServiceProducts_Client", array($this->access_key_id(),
      $this->secret_access_key(), $this->application_name(), $this->version_number(), $this->config(),
    ));
    $request = $this->spawnService("MarketplaceWebServiceProducts_Model_GetMyPriceForASINRequest");

    if ($client && $request) {
      try {
        $asin_list = $this->spawnService("MarketplaceWebServiceProducts_Model_ASINListType");
        $asin_list->setASIN($ASINList);

        if ($itemCondition) {
          $request->setItemCondition($itemCondition);
        }

        $request->setASINList($asin_list);
        $request->setSellerId($this->seller_id());
        $request->setMarketplaceId($this->marketplace_id());

        $response = $client->GetMyPriceForASIN($request);

        return $this->processResponse($response);
      }
      catch (Exception $e) {
        $this->logging->error("%function returned '%exception' as error", array(
          "%function" => __FUNCTION__,
          "%exception" => $e->getMessage(),
        ));
      }
    }

    return NULL;
  }

  /**
   * http://docs.developer.amazonservices.com/en_CA/products/Products_GetMyPriceForSKU.html.
   *
   * @param array $sellerSKUList
   * @param $itemCondition
   *
   * @return DOMDocument|null
   */
  public function GetMyPriceForSKU(array $sellerSKUList, $itemCondition = NULL) {
    $this->useLibrary("Client");
    $this->useLibrary("SellerSKUListType");
    $this->useLibrary("GetMyPriceForSKURequest");

    $client = $this->spawnService("MarketplaceWebServiceProducts_Client", array($this->access_key_id(),
      $this->secret_access_key(), $this->application_name(), $this->version_number(), $this->config(),
    ));
    $request = $this->spawnService("MarketplaceWebServiceProducts_Model_GetMyPriceForSKURequest");

    if ($client && $request) {
      try {
        $sku_list = $this->spawnService("MarketplaceWebServiceProducts_Model_SellerSKUListType");
        $sku_list->setSellerSKU($sellerSKUList);

        if ($itemCondition) {
          $request->setItemCondition($itemCondition);
        }

        $request->setSellerSKUList($sku_list);
        $request->setSellerId($this->seller_id());
        $request->setMarketplaceId($this->marketplace_id());

        $response = $client->GetMyPriceForSKU($request);

        return $this->processResponse($response);
      }
      catch (Exception $e) {
        $this->logging->error("%function returned '%exception' as error", array(
          "%function" => __FUNCTION__,
          "%exception" => $e->getMessage(),
        ));
      }
    }

    return NULL;
  }

  /**
   * http://docs.developer.amazonservices.com/en_CA/products/Products_GetProductCategoriesForASIN.html.
   *
   * @param string $ASIN
   *
   * @return DOMDocument|null
   */
  public function GetProductCategoriesForASIN($ASIN) {
    $this->useLibrary("Client");
    $this->useLibrary("GetProductCategoriesForASINRequest");

    $client = $this->spawnService("MarketplaceWebServiceProducts_Client", array($this->access_key_id(),
      $this->secret_access_key(), $this->application_name(), $this->version_number(), $this->config(),
    ));
    $request = $this->spawnService("MarketplaceWebServiceProducts_Model_GetProductCategoriesForASINRequest");

    if ($client && $request) {
      try {
        $request->setASIN($ASIN);
        $request->setSellerId($this->seller_id());
        $request->setMarketplaceId($this->marketplace_id());

        $response = $client->GetProductCategoriesForASIN($request);

        return $this->processResponse($response);
      }
      catch (Exception $e) {
        $this->logging->error("%function returned '%exception' as error", array(
          "%function" => __FUNCTION__,
          "%exception" => $e->getMessage(),
        ));
      }
    }

    return NULL;
  }

  /**
   * http://docs.developer.amazonservices.com/en_CA/products/Products_GetProductCategoriesForSKU.html.
   *
   * @param string $sellerSKU
   *
   * @return DOMDocument|null
   */
  public function GetProductCategoriesForSKU($sellerSKU) {
    $this->useLibrary("Client");
    $this->useLibrary("GetProductCategoriesForSKURequest");

    $client = $this->spawnService("MarketplaceWebServiceProducts_Client", array($this->access_key_id(),
      $this->secret_access_key(), $this->application_name(), $this->version_number(), $this->config(),
    ));
    $request = $this->spawnService("MarketplaceWebServiceProducts_Model_GetProductCategoriesForSKURequest");

    if ($client && $request) {
      try {
        $request->setSellerSKU($sellerSKU);
        $request->setSellerId($this->seller_id());
        $request->setMarketplaceId($this->marketplace_id());

        $response = $client->GetProductCategoriesForSKU($request);

        return $this->processResponse($response);
      }
      catch (Exception $e) {
        $this->logging->error("%function returned '%exception' as error", array(
          "%function" => __FUNCTION__,
          "%exception" => $e->getMessage(),
        ));
      }
    }

    return NULL;
  }

  /**
   * http://docs.developer.amazonservices.com/en_CA/products/Products_GetServiceStatus.html.
   *
   * @return array|null
   */
  public function GetServiceStatus() {
    $this->useLibrary("Client");
    $this->useLibrary("GetServiceStatusRequest");

    $client = $this->spawnService("MarketplaceWebServiceProducts_Client", array($this->access_key_id(),
      $this->secret_access_key(), $this->application_name(), $this->version_number(), $this->config(),
    ));
    $request = $this->spawnService("MarketplaceWebServiceProducts_Model_GetServiceStatusRequest");

    if ($client && $request) {
      try {
        $request->setSellerId($this->seller_id());
        $response = $client->GetServiceStatus($request);

        return $this->processResponse($response);
      }
      catch (Exception $e) {
        $this->logging->error("%function returned '%exception' as error", array(
          "%function" => __FUNCTION__,
          "%exception" => $e->getMessage(),
        ));
      }
    }

    return NULL;
  }

  /**
   * http://docs.developer.amazonservices.com/en_CA/products/Products_GetServiceStatus.html.
   *
   * @param $query
   * @param $query_context_id
   *   http://docs.developer.amazonservices.com/en_CA/products/Products_QueryContextIDs.html
   *
   * @return array|null
   */
  public function ListMatchingProducts($query = '', $query_context_id = NULL) {
    $this->useLibrary("Client");
    $this->useLibrary("ListMatchingProductsRequest");

    $client = $this->spawnService("MarketplaceWebServiceProducts_Client", array($this->access_key_id(),
      $this->secret_access_key(), $this->application_name(), $this->version_number(), $this->config(),
    ));
    $request = $this->spawnService("MarketplaceWebServiceProducts_Model_ListMatchingProductsRequest");

    if ($client && $request) {
      try {
        $request->setSellerId($this->seller_id());
        $request->setMarketplaceId($this->marketplace_id());
        $request->setQuery($query);

        if ($query_context_id) {
          $request->setQueryContextId($query_context_id);
        }

        $response = $client->listMatchingProducts($request);

        return $this->processResponse($response);
      }
      catch (Exception $e) {
        $this->logging->error("%function returned '%exception' as error", array(
          "%function" => __FUNCTION__,
          "%exception" => $e->getMessage(),
        ));
      }
    }

    return NULL;
  }

  /**
   * Returns the config required to setup the product client.
   *
   * @return array
   */
  protected function config() {
    return $this->defaultConfig("https://mws.amazonservices.com/Products/2011-10-01");
  }

}
