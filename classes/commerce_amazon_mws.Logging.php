<?php

/**
 * @file commerce_amazon_mws.Logging.php
 */

/**
 * Class CommerceAmazonMWSLogging.
 */
class CommerceAmazonMWSLogging {

  /**
   * Creates a watchdog error message.
   *
   * @param $message
   * @param $replacements
   */
  public function error($message, $replacements = array()) {
    watchdog('CommerceAmazonMWS_service', $message, $replacements, WATCHDOG_ERROR);
  }

  /**
   * Creates a watchdog warning message.
   *
   * @param $message
   * @param $replacements
   */
  public function warning($message, $replacements) {
    watchdog('CommerceAmazonMWS_service', $message, $replacements, WATCHDOG_WARNING);
  }

  /**
   * Creates a watchdog notice message.
   *
   * @param $message
   * @param $replacements
   */
  public function notice($message, $replacements) {
    watchdog('CommerceAmazonMWS_service', $message, $replacements, WATCHDOG_NOTICE);
  }

  /**
   * Creates a watchdog info message.
   *
   * @param $message
   * @param $replacements
   */
  public function info($message, $replacements) {
    watchdog('CommerceAmazonMWS_service', $message, $replacements, WATCHDOG_INFO);
  }

  /**
   * Creates a watchdog critical message.
   *
   * @param $message
   * @param $replacements
   */
  public function critical($message, $replacements) {
    watchdog('CommerceAmazonMWS_service', $message, $replacements, WATCHDOG_CRITICAL);
  }

  /**
   * Creates a watchdog debug message.
   *
   * @param $message
   * @param $replacements
   */
  public function debug($message, $replacements) {
    watchdog('CommerceAmazonMWS_service', $message, $replacements, WATCHDOG_DEBUG);
  }

}
