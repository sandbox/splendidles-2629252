# Amazon MWS
introduction
------------
This module includes; simple reporting for comparing local and amazon products, a sample feed generator and JSON
configurable feeds. This help file should help you overcome any confusion. The bulk of this module is the JSON config
file, provided you don't have a large amount of product types and display types, you should be able to keep the config
fairly small and not overly complicated, at the moment only one display type per product will work.

requirements
------------
  * Elysia Cron
  * Drupal Commerce
  * AmazonMWS libraries modified for use with this module: https://github.com/nonapod/AmazonMWS

installation
------------
  * First make sure the requirements have been met above, you must have Elysia Cron installed, along with
    Drupal Commerce.
  * Add a "libraries" directory to the module with a child directory "nonapod" inside it. If you are using composer
    you can just run composer install to take care of the Array2XML and AmazonMWS libraries, you will have to manually
    install parsedown however.
  * If you're not using composer, clone the following repository into the module's "libraries/nonapod" directory
    https://github.com/nonapod/AmazonMWS with the directory name AmazonMWS. This module will not work without them.
  * Clone Array2XML in the modules "libraries/nonapod" directory https://github.com/nonapod/Array2XML the module won't
    work without it.
  * Clone parsedown into the sites/all/libraries directory to view this file in the Drupal Help Module
    https://github.com/erusev/parsedown.
  * Enable the module from the Modules menu. 


configuration
-------------
To configure the module settings go to Store -> Configuration -> Amazon MWS Settings. The following settings must be
configured:

  * Enable Cron - If this is unchecked, the module will return straight away from the task functions when the cron
    calls them. When testing or modifying the JSON, or getting things up for the first time, you might want to keep
    this unchecked until the feed sample generator looks to be producing good XML.
  * Restrict Cron To Hostname - Sadly Amazon doesn't do sandbox environments, so if you have a few environments but
    don't want test products going up or test data to the live version, you can set this up to only work on one host.
    This is the physical server hostname not the http host, the php function used to test this is gethostname().
  * Production Settings - All of the settings here need to be set up as per your seller account on Amazon, you'll have
    to check Amazon's documentation on how to do this, but you will need the following values for Amazon to talk
    correctly.
    * AWS Access Key ID
    * Secret Access Key
    * Application Name
    * Version Number
    * Marketplace ID
    * Merchant Token
    * Seller ID
    * MWS Auth Token

The Field Mapping pane on this page will let you know if your JSON file is valid or not, if it is you will be able to
view the sample feed generator beneath it. This is all the config that you need for the module, the rest is done via
the JSON mapping file.

feed sample generator
---------------------
This is located in the admin configuration (if you have valid XML it will be visible, otherwise it will not display,
correct your JSON first) and allows you to generate sample feeds for randomly selected nodes of a given display and
feed type or a node that you choose, this will allow you to see exactly the XML that will be submitted for this node,
so if there are errors in your feed submission you should be able to check the feed sample to see what sort of data is
being submitted. This is great for debugging or when making modifications to the JSON mapping. You will need to have at
least one node with a list flag set up on it though, unless you provide an optional node id, in which case this will be
skipped, more on list flags to follow.

mapping fields
--------------
When mapping fields using the commerce_amazon_mws.map.json file, the following format for field names are enforced:

  * Fields with the prefix and suffix %, i.e. %field_product% denote a regular field attached to the node/product
    in context.

  * Fields with the prefix and suffix $, i.e. $field_size$, will take the field from the variation in context.

  * Fields with the prefix and suffix &, i.e. &node_url&, denote special computed fields, the following is a list of
    all available computed fields:
      * &node_url& - This is the clean URL of the display/parent node.
      * &unavailable_on_zero_qty& - Used in the Inventory feed, if the item is found to have 0 quantity, it is set
        to unavailable. (this currently doesn't do much)

  * Fields with no suffix and prefix are plain text.

  * When using a prefix/suffix, adding a -> and a value after it before the suffix will get a specific item within
    a field if it is available i.e. $field_product_dimensions->weight$ will get the dimensions field, and look for
    a width index inside the array if it is one, and return that value instead.

  !NOTE Only physical_weight and physical_dimensions currently work for nested fields using the following syntax
  $field_product_dimensions->weight$


json mapping
------------
Copy the commerce_amazon_mws.map.json,sample to a new file called commerce_amazon.map.json, this is the file that
must exist for this module to work. The sample is a production working sample. Expect the json to get fairly lengthy
depending on how many product display or variation product types you are using (check the XSDs and links under the
item feed categories sub header for proper formatting information). If you haven't read this guide yet, you should
https://images-na.ssl-images-amazon.com/images/G/01/rainier/help/XML_Documentation_Intl._V324496426_.pdf almost all
of the errors you will encounter will most likely stem from having something not in the correct order or a missing
required field, or perhaps you categories aren't set up according to how Amazon wants them:

  * You'll need to map the product types and for each type, with a list of display nodes. So inside product_type you
    would put something like 'product' if the machine name of the product is 'product', and the type of the node
    that displays the product needs to be listed inside the 'display_nodes' array.
      ```
      "product_types": {
        "product": {
          "display_nodes" : [
            "product_display"
          ]
        }
      }
      ```

  !NOTE: at the moment only one display_type per node works, as a lot of the logic will take only the first display
  node in the display_node array. When this was being developed it was hoped it would be more diverse, and hopefully
  one day it will be! So for now, only put one display type, per product type.

  * You'll need to map each display node types to a feed. So you will have an object called display_node, inside of
    you this you will have key for each display node type as an object, and inside here, more objects with the feed
    names as the key. Currently the following feeds are available, and should be set up (check the json sample file
    in the includes directory of this module); ProductParent, ProductParentDelete, ProductDelete, Product,
    ProductImage, Inventory, Price, Relationship.
      ```
      "display_node": {
        "product_display" : {
          "Product": {},
          "ProductImage": {},
          "Inventory": {},
        }
      }
      ```
    !NOTE: ProductParent, ProductParentDelete & ProductDelete aren't actual Amazon MWS feeds like the others, however
    are used in conjunction with the Product feed and are vital to the Product feeds working (in order to make
    relationships work properly).

  * Each of these feeds needs to have a "variation", "list_flag" and "fields" key. Note that the List flag must
    have % prefix/suffix and will not work on variation $ prefix/suffix.
      ```
      "display_node": {
        "product_display" : {
          "Product": {
            "variation": "field_product"
            "list_flag": "%field_list_on_amazon%",
            "fields": {}
          }
        }
      }
      ```

      * the variation is the node field name that holds the product variations (does not use a prefix/suffix)
      * the list_flag is a boolean flag field for either the node or variation; check the
        suffix/prefix types under the mapping fields subheading for more details on node/variation
        field mapping. The flag should be 1/TRUE for enabled and 0/FALSE for disabled, if no list_flag
        field is set for the feed mapping, all items will sync with Amazon regardless. The list flag field
        must have a % prefix and suffix to denote the parent node, this is to work with the report queueing/syncing,
        it is also Feed specific, preferably this should be set on all feeds, but if for some instance you don't
        want local stock and remote stock updates, the flag can be left out of the Inventory feed etc.
      * fields is a dictionary that holds all of the field/xml structure for the mapping, again check
        the XSDs mentioned under the item feed categories subheading for better detail on this, or check the
        sample json file in the /includes directory.

  * Categories also need to be mapped, this only takes effect when using the Product feed as it is very picky about
    what categories items are uploading to, and how the structure is formed, you will need to check Amazons Category
    Specific XSDs to understand the structure. Main categories need to be mapped under their own key and overrides
    need to be mapped under their own key, this is so any subcategories are checked first in the overrides, and then
    default up until they find a valid category. If an item is listed with multiple categories, the first found under
    the taxonomy for the item will be used, and then moving up until a valid parent is found in either an override or
    main category.
        ```
        "categories": {
          "main": {
            "Clearance" : {
              "Sports": {
                "ProductType": "SportingGoods",
                "VariationData": {
                  "VariationTheme": "ColorSize",
                  "Color": "$field_color$",
                  "Size": "$field_size$"
                }
              }
            },
          "override" : { ... follows the same structure as 'main' categories defined above }
        }
        ```
        !NOTE that the way categories are laid out must match exactly that of the category specific XDSs from Amazon,
        one categories structure may be completely different from another categories structure.

      * alternate SKUs can be used, this is in case a different SKU is being used locally than what is on Amazon,
        to use this, create an alternate sku field on the variation and parent product, and add it to the JSON
        in the top level of the feed, outside of the fields. the alternate sku will always override the sku field,
        that is currently set if an alternate SKU exists. This allows for some items of regular SKU to sync, and
        some alternate SKU items to exist;

        * for the parent sku, add an alternate_parent_sku key/val pair !NOTE the field must be a regular text field
          ```
          "ProductParent": {
            "list_flag": "...",
            "alternate_parent_sku": "%field_alternate_sku%",
            "fields": {...}
          }
          ```

        * for the variation sku, add an alternate_sku key/val pair !NOTE the field must be a regular text field
          ```
          "ProductParent": {
            "list_flag": "...",
            "alternate_sku": "$field_alternate_sku$",
            "fields": {...}
          }
          ```
        * the alternate-parent-sku works by replacing any SKU or ParentSKU field that has the % prefix and
          suffix, denoting the parent node, whereas the alternate-sku works by replacing any SKU field that has
          the $ prefix and suffix, denoting the product variation.



  !NOTE It's advisable to set the list_flag field on your nodes and in the mapping to give control,
    otherwise it is a free-for-all for all items, and everything will be synced.


conditionals
------------
these are in an early stage of development, a "conditionals" object should be added at the top level, for each
display node inside a field name should be set, per per conditional. Inside here (only 'if' works for now), an 'if'
array should go, (only 'not' works as a conditional at the moment) a dictionary per 'if' conditional needs to go inside
of the array. The following is an example:
```
"conditionals": {
  "product_display": {
    "VariationData": {
      "if": [
        {
          "not": "Size",
          "then": {
            "set": [
              ["VariationTheme", "Color"]
            ]
          }
        }
      ]
    }
  }
}
```

here, whenever the field VariationData is encountered in the product_display type, it will check if there is no
instance of Size, if there isn't then it will set the VariationTheme to Color. This was necessary as Amazon MUST
get a Size and a Color on a SizeColor VariationTheme, this allows the module to adjust should only a Color or a Size
appear, so it can change the VariationTheme dynamically and not have Amazon return an error.

!NOTE this is still in early stages so it only has minimal features pertaining to a specific need that was met with its creation.

json feed types
---------------
the json feed types that should be set up at minimal are, again please refer to the documents previously referenced in
this help file for information about the feed types, along with the sample json config file for more information:
  * Inventory
  * Product
  * ProductParent
  * ProductDelete
  * ProductImages
  * Price
  * Relationship

* the ProductParent feed is used in conjunction with Product and not called directly, it is injected into the
  Product feed as the parent display node of the variations. Title, Brand, Description and Bulletpoint are recommended
  fields. the VariationData field is all taken care of during the Product generation, check the Feed Sample Generator
  in admin by generating a Product feed type and check the first item contains a ProductParent.


item feed categories
--------------------
Mapping item feed categories requires the following list of Category-Specific XSDs from Amazon:
* https://sellercentral.amazon.com/gp/help/200385010

Some elements also can only have specific values, check the following for a list of available:
* https://sellercentral.amazon.com/gp/help/help.html/ref=ag_35241_bred_35251?ie=UTF8&itemID=35241&language=en_US

Use arrays in JSON if you need more than one of the same tag; tag repetition for multiple value, i.e.
* ```"BulletPoint": ["$value$", "$value$", "$value$"];```

A Product Type is needed to create a new ASIN, use the following to get a list of product types:
* https://sellercentral.amazon.com/gp/help/help-page.html/ref=ag_1661_cont_scsearch?ie=UTF8&itemID=1661

When setting up a mapping for a feed, fields have to appear precisely in the right order as laid out in the XSDs, i.e.
the Product XSD must be laid out exactly or else you will get a sequence error.

    !It may seem logical just to KSORT an entire object once generated, however this will not work as certain elements
    must appear before due to the complex sequence nature of the XML service. Check the Amazon MWS XML guides if you're
    getting errors saying a particular element was expected.

categories have to be mapped with the parent name as an object, with the mapped name and attributes nested within it,
i.e.
```
"categories": {
  "main": {
    "scooters": {
      "name": "Toys",
      "VariationTheme": {
        "Color": "$field_product_color$",
        "Size": "$field_product_dimensions$"
      },
      "ProductType": {
        "name": "Hobbies",
        "data": {
          "WheelType": "$field_scooter_wheel_type$",
          "WheelDiameter": "$field_scooter_wheel_diameter$"
        }
      }
    }
  }
}
```

check the XSDs and the samples, the structure should be fairly self explanatory.

feed submission error checking
------------------------------
Currently there is none! You will have to use the Amazon Sandbox for this, this is something that could probably very
easily be implemented, but hasn't yet, go here for now: http://stackoverflow.com/questions/8894782/amazon-mws-sandbox

If you are scratching your head wondering what's wrong with your feed, check the Feed Sample Generator in the admin
panel to look at the type of content that is being pushed, you will have to do your own detective work, cross
referencing with the Amazon XSD documentation. If however, you're absolutely certain the problem is with the module and
not your feed config, please get in touch!

# Developers
Adding New APIs and Existing APIs
------------
Currently only the following Amazon MWS APIs are implemented:
  * The Products API (https://developer.amazonservices.ca/gp/mws/api.html/186-7324556-6152127?ie=UTF8&group=products&section=products&version=latest)
  * The Feeds API (https://developer.amazonservices.ca/doc/bde/feeds/v20090101/php.html/186-7324556-6152127)
  * The Reports API (https://developer.amazonservices.ca/doc/bde/reports/v20090101/php.html/190-0932076-0951510)

They are located in the modules libraries/AmazonMWS directory with the following structure:

      * AmazonMWS/MarketplaceWebService[API Name Here]/
        * Mock/
        * Model/
        * Samples/
        * Client.php
        * Exception.php
        * Interface.php
        * Mock.php
        * Model.php

If you wish to add a new API, download it and unpack it using the aforementioned structure. You will also need to add a
new class, which will be a child class of the commerce_amazon_mws.Service.php class. Check the following files to see
how these are implemented:

    * classes/commerce_amazon_mws.Feeds.php
    * classes/commerce_amazon_mws.Products.php
    * classes/commerce_amazon_mws.Reports.php

    !NOTE The Feeds and Reports API were broken by default, paths were not correct in require statements and there
    were some errors with with Model.php class. They have been fixed in the included implementation, but when
    downloading directly from Amazon to add a new API implementation, be aware some work will be required.

