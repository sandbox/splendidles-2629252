/**
 * @file
 */

(function ($) {

  'use strict';

  if ($) {
    $(function () {

      // REPORT DISPLAY FUNCTIONALITY.
      var report_table = $('#amazon-mws-report');
      // Filter by row key on click.
      $('.row-keys .row-key').click(function () {
        var table = $('#amazon-mws-report tbody');
        table.find('tr').hide();
        ($(this).hasClass('show-all')) && (table.find('tr').show());
        ($(this).hasClass('no-issues')) && (table.find('.no-issues').show());
        ($(this).hasClass('no-sku-match')) && (table.find('.no-sku-match').show());
        ($(this).hasClass('title-mismatch')) && (table.find('.title-mismatch').show());
        ($(this).hasClass('not-listed')) && (table.find('.not-listed').show());

      });

      // DELETE REPORT FUNCTIONALITY.
      var delete_report_buttons = $('.commerce_amazon_mws--delete-reports');
      var access_token = $('#commerce_amazon_mws_access_token').val();
      if (delete_report_buttons.length) {
        delete_report_buttons.click(function (event) {
          event.preventDefault();
          var selected_reports = $('input[data-report-id]:checked');
          if (selected_reports.length) {
            // Delete the selected reports.
            selected_reports.each(function (e) {
              var context = $(this);
              var report_id = context.attr("data-report-id");
              var report_type = context.attr("data-report-type");
              $.get("/admin/reports/commerce_amazon_mws/delete?report_id=" + report_id + "&report_type=" + report_type + "&commerce_amazon_mws_access_token=" + access_token,
                function (res) {
                  if (res.ok) {
                    context.parent().parent().addClass("row-deleted");
                  }
                })
            });
          }
        });
      }

    });

  }

})(jQuery);
